#ifndef MONSTER_TILE_H
#define MONSTER_TILE_H

struct tile_size {
    unsigned int sx, sy, ex, ey, width, height, offset_x, offset_y;
};
tile_size get_tile_size();

extern const char *g_output_filename;

#endif
