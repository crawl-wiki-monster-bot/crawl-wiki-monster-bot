
require 'string'
-- 0.13 changes: remove SE, T&D and Stabbing; add Gr, LO, Dj

-- table with the functions of this module
local p = {}

-- species
local species_ab = {'Hu', 'Fe', 'Ce', 'Dg', 'Dj', 'Ds', 'Dr', 'DD', 'DE', 'HE', 'Fo', 'Gr',
		    'Gh', 'Ha', 'Ko', 'Mf', 'Mi', 'Mu', 'Na', 'Op', 'Og', 'HO', 'Sp', 'Te', 'Tr', 'Vp', 'VS'};
local species_full = {'Human', 'Felid', 'Centaur', 'Demigod', 'Djinni', 'Demonspawn', 'Base Draconian',
                    'Deep Dwarf', 'Deep Elf', 'High Elf', 'Formicid', 'Gargoyle', 'Ghoul',
                    'Halfling', 'Kobold', 'Merfolk', 'Minotaur', 'Mummy', 'Naga',
                    'Octopode', 'Ogre', 'Hill Orc', 'Spriggan', 'Tengu', 'Troll', 'Vampire', 'Vine Stalker'}
local species_wiki = {'Human', 'Felid', 'Centaur', 'Demigod', 'Djinni', 'Demonspawn', 'Draconian',
                    'Black Draconian', 'Green Draconian', 'Grey Draconian', 'Mottled Draconian',
                    'Pale Draconian', 'Purple Draconian', 'Red Draconian', 'White Draconian', 'Yellow Draconian',
                    'Deep Dwarf', 'Deep Elf', 'Formicid', 'High Elf', 'Gargoyle', 'Ghoul',
                    'Halfling', 'Kobold', 'Merfolk', 'Minotaur', 'Mummy', 'Naga',
                    'Octopode', 'Ogre', 'Hill Orc', 'Spriggan', 'Tengu', 'Troll', 'Vampire', 'Vine Stalker'}
local species_game = {'Human', 'Felid', 'Centaur', 'Demigod', 'Djinni', 'Demonspawn', 'Base Draconian',
                    'Black Draconian', 'Green Draconian', 'Grey Draconian', 'Mottled Draconian',
                    'Pale Draconian', 'Purple Draconian', 'Red Draconian', 'White Draconian', 'Yellow Draconian',
                    'Deep Dwarf', 'Deep Elf', 'Formicid', 'High Elf', 'Gargoyle', 'Ghoul',
                    'Halfling', 'Kobold', 'Merfolk', 'Minotaur', 'Mummy', 'Naga',
                    'Octopode', 'Ogre', 'Hill Orc', 'Spriggan', 'Tengu', 'Troll', 'Vampire', 'Vine Stalker'}
local species_game_013 = {'Human', 'Felid', 'Centaur', 'Demigod', 'Djinni', 'Demonspawn', 'Base Draconian',
                    'Black Draconian', 'Green Draconian', 'Grey Draconian', 'Mottled Draconian',
                    'Pale Draconian', 'Purple Draconian', 'Red Draconian', 'White Draconian', 'Yellow Draconian',
                    'Deep Dwarf', 'Deep Elf', 'High Elf', 'Formicid', 'Gargoyle', 'Ghoul',
                    'Halfling', 'Kobold', 'Merfolk', 'Minotaur', 'Mummy', 'Naga',
                    'Octopode', 'Ogre', 'Hill Orc', 'Spriggan', 'Tengu', 'Troll', 'Vampire', 'Vine Stalker'}

--[[
Ordered table iterator, allow to iterate on the natural order of the keys of a
table.

Example:
]]

function __genOrderedIndex( t )
    local orderedIndex = {}
    for key in pairs(t) do
        table.insert( orderedIndex, key )
    end
    table.sort( orderedIndex )
    return orderedIndex
end

function orderedNext(t, state)
    -- Equivalent of the next function, but returns the keys in the alphabetic
    -- order. We use a temporary ordered key table that is stored in the
    -- table being iterated.

    --print("orderedNext: state = "..tostring(state) )
    if state == nil then
        -- the first time, generate the index
        t.__orderedIndex = __genOrderedIndex( t )
        key = t.__orderedIndex[1]
        return key, t[key]
    end
    -- fetch the next value
    key = nil
    for i = 1,#t.__orderedIndex do
        if t.__orderedIndex[i] == state then
            key = t.__orderedIndex[i+1]
        end
    end

    if key then
        return key, t[key]
    end

    -- no more value to return, cleanup
    t.__orderedIndex = nil
    return
end

function orderedPairs(t)
    -- Equivalent of the pairs() function on tables. Allows to iterate
    -- in order
    return orderedNext, t, nil
end


function p._fixcase(s)
   local result = ""
   local capital = true
   for i=1,string.len(s) do
      local c = string.sub(s, i, i)
      if capital then
	 result = result .. string.upper(c)
      else
	 result = result .. string.lower(c)
      end
      if (c == " ") then
	 capital = true
      else
	 capital = false
      end
   end
   return result
end

function p._adjust(s)
   local s_with_spaces = string.gsub(s, "_", " ")
   return p._fixcase(s_with_spaces)
end

function p._adjust_species_name(sp)
   return p._adjust(sp)
end

function p._adjust_skill_name(sk)
   return p._adjust(sk)
end


function p._get_letter(i)
   local m = string.byte('z') - string.byte('a')
   local t, c
   if i > m then
      t = string.byte('A') + i - m - 1
      c = '"'
   else
      t = string.byte('a') + i
      c = '!'
   end
   return c .. string.char(t)
end

function p._load_modifiers(aptitudes)
   local species_file = "./crawl-ref/crawl-ref/source/species.cc"
   local f = assert(io.open(species_file, 'r'))
   local skill, species = nil, {}

   -- init to zero
   local skills = { "Experience", "Hit Points", "Magic Points"}
   local i
   for _, i in ipairs(species_game_013) do
      if not aptitudes[i] then
	 aptitudes[i] = {}
      end
      local s
      for _, s in ipairs(skills) do
	 aptitudes[i][s] = "0"
      end
   end

   local line = f:read('*line')
   while line do
      local c  = string.match(line, "// table: ([^\n]+)")
      if c then
	 skill = c
      end
      if skill then
	 local name = string.match(line, "SP_([%w_]+):")
	 if name then
	    table.insert(species, p._adjust(name))
	 end
	 local value = string.match(line, "return (%-?%d+)")
	 if value then
	    local sp
	    for _,  sp in ipairs(species) do
	       if not aptitudes[sp] then
		  aptitudes[sp] = {}
	       end
	       aptitudes[sp][skill] = value
	    end
	    species = {}
	 end
      end
      if string.find(line, "}") then
	 skill = nil
      end
      -- next line
      line = f:read('*line')
   end -- while
end

function p._load_aptitudes(aptitudes)
   local skill_file = "./crawl-ref/crawl-ref/source/aptitudes.h"
   local f = assert(io.open(skill_file, 'r'))
   local skill_start
   local skip = false
   if not aptitudes then
      aptitudes = {}
   end

   local line = f:read('*line')
   while line do
      local m = string.match(line, '(%**)')
      if m and string.len(m) >= 40 then
	 return
      end
      if not skill_start then
	 if string.match(line, 'species_skill_aptitudes%[') then
	    skill_start = true
	 end
      else
	 if string.match(line, "%#if TAG") then
	    skip = true
	 elseif string.match(line, "#endif") then
	    skip = false
	 elseif not skip then
	    local c1, c2, c3 = string.match(line, 'APT%(%s*SP_([%w_]+)%s*,%s*SK_([%w_]+)%s*,%s*(%-?%d+)%s*%)')
	    if c1 then
	       local sp = p._adjust_species_name(c1)
	       local sk = p._adjust_skill_name(c2)
	       local v = c3
	       if v == "-99" then
		  v = "N/A"
	       end
	       if not aptitudes[sp] then
		  aptitudes[sp] = {}
	       end
	       aptitudes[sp][sk] = v
	    end
	 end
      end
      -- next line
      line = f:read('*line')
      end --while
   return
end -- p._load_aptitudes

function p._demo()
   --print(p.aptitudechart_general())
   local aptitudes = {}
   p._load_aptitudes(aptitudes)
   p._load_modifiers(aptitudes)
   print("--[=[")
   print([==[
    This module contains a table of aptitudes.]==])
   print("]=]--\n")
   print("local m = {}\n")
   for sp,v in orderedPairs(aptitudes) do
      print("m[\"" .. sp .. "\"] = {")
      for sk, apt in orderedPairs(v) do
	 print("  [\"" .. sk .. "\"] = \"" .. apt .. "\",")
      end
      print("}")
   end
   print("return m")
end

p._demo()

return p