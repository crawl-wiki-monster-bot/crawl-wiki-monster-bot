/*
 * ===========================================================================
 * Copyright (C) 2007 Marc H. Thoben
 * Copyright (C) 2008 Darshan Shaligram
 * Copyright (C) 2010 Jude Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ===========================================================================
 */

#include "AppHdr.h"
#include "artefact.h"
#include "externs.h"
#include "directn.h"
#include "unwind.h"
#include "env.h"
#include "butcher.h"
#include "colour.h"
#include "dungeon.h"
#include "database.h"
#include "describe.h"
#include "options.h"
#include "initfile.h"
#include "los.h"
#include "message.h"
#include "mon-abil.h"
#include "mon-cast.h"
#include "mon-place.h"
#include "mon-util.h"
#include "version.h"
#include "view.h"
#include "los.h"
#include "maps.h"
#include "initfile.h"
#include "libutil.h"
#include "itemname.h"
#include "itemprop.h"
#include "act-iter.h"
#include "misc.h"
#include "mon-death.h"
#include "random.h"
#include "spl-util.h"
#include "state.h"
#include "stepdown.h"
#include "stringutil.h"
#include "vault_monsters.h"
#include <sstream>
#include <set>
#include <unistd.h>

const coord_def MONSTER_PLACE(20, 20);
const coord_def PLAYER_PLACE(21, 20);

const std::string CANG = "cang";

const int PLAYER_MAXHP = 500;
const int PLAYER_MAXMP = 50;

bool g_show_enchpower = false;

// Clockwise, around the compass from north (same order as enum RUN_DIR)
const struct coord_def Compass[9] =
{
    coord_def(0, -1), coord_def(1, -1), coord_def(1, 0), coord_def(1, 1),
    coord_def(0, 1), coord_def(-1, 1), coord_def(-1, 0), coord_def(-1, -1),
    coord_def(0, 0),
};

bool is_element_colour(int col)
{
    col = col & 0x007f;
    ASSERT(col < NUM_COLOURS);
    return (col >= ETC_FIRE);
}


static std::string colour_codes[] = {
    "",
    "02",
    "03",
    "10",
    "05",
    "06",
    "07",
    "15",
    "14",
    "12",
    "09",
    "11",
    "04",
    "13",
    "08",
    "16"
};

static int bgr[8] = { 0, 4, 2, 6, 1, 5, 3, 7 };

#ifdef CONTROL
#undef CONTROL
#endif
#define CONTROL(x) char(x - 'A' + 1)

static std::string colour(int colour, std::string text, bool bg = false)
{
    if (is_element_colour(colour))
        colour = element_colour(colour, true);

    if (isatty(1))
    {
        if (!colour)
            return text;
        return make_stringf("\e[0;%d%d%sm%s\e[0m", bg ? 4 : 3, bgr[colour & 7],
                            (!bg && (colour & 8)) ? ";1" : "", text.c_str());
    }

    const std::string code(colour_codes[colour]);

    if (code.empty())
        return text;

    return (std::string() + CONTROL('C') + code + (bg ? ",01" : "")
            + text + CONTROL('O'));
}

static std::string colour_names[] = {
    "Black",
    "Blue",
    "Green",
    "Cyan",
    "Red",
    "Magenta",
    "Brown",
    "LightGrey",
    "DarkGrey",
    "LightBlue",
    "LightGreen",
    "LightCyan",
    "LightRed",
    "LightMagenta",
    "Yellow",
    "White"
};

std::string colour_to_string(int colour)
{
    if (is_element_colour(colour))
        colour = element_colour(colour, true);
    return colour_names[colour];
}

static std::string convert_to_roman (unsigned int val) {
    const char *huns[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
    const char *tens[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
    const char *ones[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
    std::string r;
    //  Add 'M' until we drop below 1000.

    while (val >= 1000) {
        r += std::string(1,'M');
        val -= 1000;
    }

    // Add each of the correct elements, adjusting as we go.

    r += huns[val/100]; val = val % 100;
    r += tens[val/10]; val = val % 10;
    r += ones[val];

    return r;
}

std::string uppercase_first(std::string s);

template <class T> inline std::string to_string (const T& t);

static void record_resvul(const char *name, const char *caption, const char *caption2,
                          std::string &str, int rval, int total)
{
  if (!str.empty())
    str += ", ";

  if (total > 0 && total%2 == 0)
    str += "<br>";

  str += "{{";
  std::stringstream token;
  token << name;
  token << " " << caption2;
  if (rval > 1 && rval <= 3) {
    token << " " << rval;
  }
  str += token.str();
  str += "}}";
}

static void record_resist(std::string name,
                          std::string &res, std::string &vul,
                          int rval)
{
  static int total_res=0;
  static int total_vul=0;
  if (rval > 0)
    record_resvul(name.c_str(), "resistances", "resistance", res, rval, total_res++);
  else if (rval < 0)
    record_resvul(name.c_str(), "vulnerabilities", "vulnerability", vul, -rval, total_vul++);
}

static void monster_action_cost(std::string &qual, int cost, const char *desc) {
  char buf[80];
  if (cost != 10) {
    snprintf(buf, sizeof buf, "%s: %d%%", desc, cost * 10);
    if (!qual.empty())
      qual += "; ";
    qual += buf;
  }
}

static std::string monster_int(const monster &mon)
{
  std::string intel = "???";
  switch (mons_intel(&mon))
  {
  case I_PLANT:
    intel = "{{Plant intelligence}}";
    break;
  case I_INSECT:
    intel = "{{Insect intelligence}}";
    break;
  case I_REPTILE:
    intel = "{{Reptile intelligence}}";
    break;
  case I_ANIMAL:
    intel = "{{Animal intelligence}}";
    break;
  case I_NORMAL:
    intel = "{{Normal intelligence}}";
    break;
  case I_HIGH:
    intel = "{{High intelligence}}";
    break;
  // Let the compiler issue warnings for missing entries.
  }

  return intel;
}

static std::string monster_size(const monster &mon)
{
  switch (mon.body_size())
  {
  case SIZE_TINY:
    return "tiny";
  case SIZE_LITTLE:
    return "little";
  case SIZE_SMALL:
    return "small";
  case SIZE_MEDIUM:
    return "Medium";
  case SIZE_LARGE:
    return "Large";
  case SIZE_BIG:
    return "Big";
  case SIZE_GIANT:
    return "Giant";
  default:
    return "???";
  }
}

static std::string monster_speed(const monster &mon,
                                 const monsterentry *me,
                                 int speed_min,
                                 int speed_max)
{
  std::string speed;

  char buf[50];
  if (speed_max != speed_min)
    snprintf(buf, sizeof buf, "%i-%i", speed_min, speed_max);
  else
    snprintf(buf, sizeof buf, "%i", speed_max);

  speed += buf;

  const mon_energy_usage &cost(me->energy_usage);
  std::string qualifiers;

  bool skip_action = false;
  if (cost.attack != 10
      && cost.attack == cost.missile && cost.attack == cost.spell
      && cost.attack == cost.special && cost.attack == cost.item)
  {
    monster_action_cost(qualifiers, cost.attack, "act");
    skip_action = true;
  }

  monster_action_cost(qualifiers, cost.move, "move");
  if (cost.swim != cost.move)
    monster_action_cost(qualifiers, cost.swim, "swim");
  if (!skip_action)
  {
    monster_action_cost(qualifiers, cost.attack, "atk");
    monster_action_cost(qualifiers, cost.missile, "msl");
    monster_action_cost(qualifiers, cost.spell, "spell");
    monster_action_cost(qualifiers, cost.special, "special");
    monster_action_cost(qualifiers, cost.item, "item");
  }

  if (!qualifiers.empty())
    speed += " (" + qualifiers + ")";

  return speed;
}

static void mons_flag(std::string &flag, const std::string &newflag) {
  if (!flag.empty())
    flag += "<br>";
  flag += newflag;
}

static void mons_check_flag(bool set, std::string &flag,
                            const std::string &newflag)
{
  if (set)
    mons_flag(flag, newflag);
}

static void initialize_crawl() {
  get_system_environment();
  SysEnv.crawl_dir = "crawl-ref/crawl-ref/source/";

  init_monsters();
  init_properties();
  init_item_name_cache();

  init_spell_descs();
  init_monster_symbols();
  databaseSystemInit();
  init_mon_name_cache();
  init_spell_name_cache();
  init_mons_spells();
  init_element_colours();
  init_show_table();

  dgn_reset_level();
  for (int y = 0; y < GYM; ++y)
    for (int x = 0; x < GXM; ++x)
      grd[x][y] = DNGN_FLOOR;

  los_changed();
  you.moveto(PLAYER_PLACE);
  you.hp = you.hp_max = PLAYER_MAXHP;
  you.magic_points = you.max_magic_points = PLAYER_MAXMP;
  you.species = SP_HUMAN;

}

static std::string dice_def_string(dice_def dice) {
  return (dice.num == 1? make_stringf("d%d", dice.size)
          : make_stringf("%dd%d", dice.num, dice.size));
}

static dice_def mi_calc_iood_damage(monster *mons) {
  const int power = stepdown_value(6 * mons->spell_hd(SPELL_IOOD),
                                   30, 30, 200, -1);
  return dice_def(9, power / 4);
}

static std::string mi_calc_smiting_damage(monster *mons) {
  return "7-17";
}

static std::string mi_calc_airstrike_damage(monster *mons) {
  return make_stringf("0-%d", 10 + 2 * mons->spell_hd(SPELL_AIRSTRIKE));
}

static std::string mi_calc_waterstrike_damage(monster *mons) {
  return make_stringf("3d%d", 7 + mons->spell_hd(SPELL_WATERSTRIKE));
}

static std::string mi_calc_major_healing_damage(monster *mons) {
  return make_stringf("50-%d", 50 - 1 + mons->spell_hd(SPELL_MAJOR_HEALING) * 10);
}

static std::string mi_calc_glaciate_damage(monster *mons) {
  int pow = 12 * mons->get_experience_level();
  // Minimum of the number of dice, or the max damage at max range
  int min = std::min(10, (54 + 3 * pow / 2) / 6);
  // Maximum damage at minimum range.
  int max = (54 + 3 * pow / 2) / 3;

  return make_stringf("%d-%d", min, max);
}

static std::string mons_human_readable_spell_damage_string(
    monster *monster,
    spell_type sp)
{
    bolt spell_beam = mons_spell_beam(monster, sp, 12 * monster->spell_hd(sp),
                                      true);
    if (sp == SPELL_LEGENDARY_DESTRUCTION || sp == SPELL_MAJOR_DESTRUCTION
        || sp == SPELL_RANDOM_BOLT || sp == SPELL_LRD || sp == SPELL_PORTAL_PROJECTILE)
        return "";

    if (spell_beam.ench_power == -1) {
        // Copy from the beginning of setup_mons_cast
        spell_beam.ench_power = 4 * monster->spell_hd(sp);

        if (sp == SPELL_TELEPORT_SELF)
            spell_beam.ench_power = 2000;
        else if (sp == SPELL_SLEEP || sp == SPELL_AGONY || sp == SPELL_VIRULENCE)
            spell_beam.ench_power = 6 * monster->spell_hd(sp);
        else if (sp == SPELL_IGNITE_POISON_SINGLE)
            spell_beam.ench_power = 12 * monster->spell_hd(sp);
        else if (sp == SPELL_SAP_MAGIC)
            spell_beam.ench_power = 10 * monster->spell_hd(sp);
        else if (sp == SPELL_DRAIN_MAGIC)
            spell_beam.ench_power = 6 * monster->spell_hd(sp);
        else if (sp == SPELL_SENTINEL_MARK)
            spell_beam.ench_power = 125;
        else if (sp == SPELL_PAIN)
            spell_beam.ench_power = max(50, 8 * monster->spell_hd(sp));
        else if (sp == SPELL_DIMENSION_ANCHOR)
            spell_beam.ench_power = spell_beam.ench_power / 2;
    }

    if (sp == SPELL_SMITING)
        return make_stringf(" (%s)", mi_calc_smiting_damage(monster).c_str());
    if (sp == SPELL_AIRSTRIKE)
        return make_stringf(" (%s)", mi_calc_airstrike_damage(monster).c_str());
    if (sp == SPELL_WATERSTRIKE)
        return make_stringf(" (%s)", mi_calc_waterstrike_damage(monster).c_str());
    if (sp == SPELL_GLACIATE)
        return make_stringf(" (%s)", mi_calc_glaciate_damage(monster).c_str());
    if (sp == SPELL_MAJOR_HEALING)
        return make_stringf(" (%s)", mi_calc_major_healing_damage(monster).c_str());
    if (sp == SPELL_IOOD)
        spell_beam.damage = mi_calc_iood_damage(monster);
    if (spell_beam.damage.size && spell_beam.damage.num)
        return make_stringf(" (%s)", dice_def_string(spell_beam.damage).c_str());

    // see bolt::has_saving_throw()
    if (g_show_enchpower
        && (spell_beam.is_enchantment()
            && spell_beam.flavour != BEAM_MALMUTATE
            && spell_beam.flavour != BEAM_CORRUPT_BODY
            && spell_beam.flavour != BEAM_SAP_MAGIC
            && spell_beam.flavour != BEAM_HASTE
            && spell_beam.flavour != BEAM_MIGHT
            && spell_beam.flavour != BEAM_BERSERK
            && spell_beam.flavour != BEAM_HEALING
            && spell_beam.flavour != BEAM_INVISIBILITY
            && spell_beam.flavour != BEAM_DISPEL_UNDEAD
            && spell_beam.flavour != BEAM_ENSLAVE_SOUL
            && spell_beam.flavour != BEAM_BLINK_CLOSE
            && spell_beam.flavour != BEAM_BLINK
            && spell_beam.flavour != BEAM_MALIGN_OFFERING
            && sp != SPELL_TELEPORT_SELF
            && sp != SPELL_IGNITE_POISON_SINGLE
            && sp != SPELL_IGNITE_POISON
            && sp != SPELL_VIRULENCE
            && sp != SPELL_STRIP_RESISTANCE
            && sp != SPELL_DIG
            || spell_beam.flavour == BEAM_VULNERABILITY)) {
        int power = spell_beam.ench_power;
        power = stepdown_value(power, 30, 40, 100, 120);
        return make_stringf(" {{Enchpower|%s|%d}}", spell_title(sp), power);
    }

    return ("");
}

static std::string shorten_spell_name(std::string name) {
  lowercase(name);
  std::string::size_type pos = name.find('\'');
  if (pos != std::string::npos ) {
    pos = name.find(' ', pos);
    if (pos != std::string::npos)
    {
      if (starts_with(name, "death's"))
        name = "d." + name.substr(pos + 1);
      else if (!starts_with(name, "trog's"))
        name = name.substr(pos + 1);
    }
  }
  if ((pos = name.find(" of ")) != std::string::npos)
    name = name.substr(pos + 4) + " " + name.substr(0, pos);
  if (name.find("summon ") == 0 && name != "summon undead")
    name = name.substr(7);
  if (name.find("bolt") == name.length() - 4)
    name = "b." + name.substr(0, name.length() - 5);
  return (name);
}

static void mons_record_ability(std::set<std::string> &ability_names,
                                monster *monster)
{
  no_messages mx;
  bolt beam;
  you.hp = you.hp_max = PLAYER_MAXHP;
  monster->moveto(MONSTER_PLACE);
  you.moveto(PLAYER_PLACE);
  mon_special_ability(monster, beam);
  if (monster->pos() != MONSTER_PLACE)
    beam.name = "blink";
  if (you.hp == PLAYER_MAXHP / 2 + 1)
    beam.name = "symbol of torment";
  if (monster->type != MONS_TWISTER) {
    for (monster_iterator mi; mi; ++mi) {
      if (mi->type == MONS_TWISTER) {
        beam.name = "summon twister";
        monster_die(*mi, KILL_RESET, -1, true);
      }
    }
  }
  if (!beam.name.empty()) {
    std::string ability = shorten_spell_name(beam.name);
    if (beam.damage.num && beam.damage.size) {
      std::string extra;
      // Skip the shield slot when reckoning acid damage.
      if (ability == "acid splash")
        extra = "+" +
          dice_def_string(dice_def(EQ_MAX_ARMOUR - EQ_MIN_ARMOUR + 2, 5));
      ability += make_stringf(" (%s%s)",
                              dice_def_string(beam.damage).c_str(),
                              extra.c_str());
    }
    ability_names.insert(ability);
  }
}

static std::string mons_special_ability_set(monster *monster) {
  if (mons_genus(monster->type) == MONS_DRACONIAN
      && draco_or_demonspawn_subspecies(monster) != MONS_YELLOW_DRACONIAN)
  {
    return ("");
  }

  // Try X times to get a list of abilities.
  std::set<std::string> abilities;
  for (int i = 0; i < 50; ++i)
    mons_record_ability(abilities, monster);
  if (abilities.empty())
    return ("");
  return comma_separated_line(abilities.begin(), abilities.end(), ", ", ", ");
}

static spell_type mi_draconian_breath_spell(monster *mons) {
  if (mons_genus(mons->type) != MONS_DRACONIAN)
    return SPELL_NO_SPELL;
  switch (draco_or_demonspawn_subspecies(mons)) {
  case MONS_DRACONIAN:
  //case MONS_YELLOW_DRACONIAN:
  case MONS_GREY_DRACONIAN:
    return SPELL_NO_SPELL;
  default:
    return SPELL_DRACONIAN_BREATH;
  }
}

static std::string mons_spell_set2(monster *mp) {
  std::string spells;
  bool spellcaster = false;

  for (size_t i = 0; i < mp->spells.size(); ++i) {
    const spell_type sp = mp->spells[i].spell;
    std::stringstream line;
    if (sp != SPELL_NO_SPELL) { 
        spellcaster = true;
        std::string rawname = spell_title(sp);
        if (sp == SPELL_MELEE)
            rawname = std::string("Melee spell|Melee");
        if (sp == SPELL_MIASMA_BREATH)
            rawname = std::string("Miasma (monster spell)|Miasma");
        std::string damage = mons_human_readable_spell_damage_string(mp, sp);
        line << "|slot" << i + 1 << "=[[" << rawname << "]]" << damage <<"\n";
    } else {
        line << "|slot" << i + 1 << "=''none''\n";
    }
    spells += line.str();
  }
  if (spellcaster)
    return spells;
  else
    return std::string("");
}

static void record_spell_set2(monster *mp,
                             std::set<std::string> &sets)
{
  std::string spell_set = mons_spell_set2(mp);
  if (!spell_set.empty())
    sets.insert(spell_set);
}

static std::string mons_spells_abilities(
  monster *monster,
  bool shapeshifter,
  const std::set<std::string> &spell_sets)
{
  if (shapeshifter || monster->type == MONS_PANDEMONIUM_LORD)
    return "(random)";

  bool first = true;
  std::string spell_abilities = mons_special_ability_set(monster);
  for (std::set<std::string>::const_iterator i = spell_sets.begin();
       i != spell_sets.end(); ++i)
  {
    if (!first)
      spell_abilities += " / ";
    else if (!spell_abilities.empty())
      spell_abilities += "; ";
    first = false;
    spell_abilities += *i;
  }
  return (spell_abilities);
}

static std::string mons_spells_abilities2(
  monster *monster,
  std::string name,
  bool shapeshifter,
  const std::set<std::string> &spell_sets)
{
  if (shapeshifter || monster->type == MONS_PANDEMONIUM_LORD)
  {
    std::string r = std::string("{{Spellcaster\n") + 
                    std::string("|number=\n") +
                    std::string("|slot1=''random''\n") +
                    std::string("|slot2=''random''\n") +
                    std::string("|slot3=''random''\n") +
                    std::string("|slot4=''random''\n") +
                    std::string("|slot5=''random''\n") +
                    std::string("|slot6=''random''\n") +
                    std::string("}}\n");
    return r;
  }

  std::string spell_abilities;
  const int sz = spell_sets.size();
  unsigned k = 1;
  for (std::set<std::string>::const_iterator i = spell_sets.begin();
       i != spell_sets.end(); ++i)
  {
    if (sz > 1)
      spell_abilities += "{{Spellcaster\n|number=" + convert_to_roman(k) + "\n";
    else
      spell_abilities += "{{Spellcaster\n|number=\n";
    spell_abilities += *i;
    spell_abilities += "}}\n";
    if (sz>1 && k%2==0)
      spell_abilities += "{{clear}}\n";
    k++;
  }
  if (k%2==0)
    spell_abilities += "{{clear}}\n";
  return (spell_abilities);
}

static inline void set_min_max(int num, int &min, int &max) {
  if (!min || num < min)
    min = num;
  if (!max || num > max)
    max = num;
}

typedef std::pair<std::string, std::string> symbol;
static symbol monster_symbol(const monster &mon) {
  symbol symbol;
  const monsterentry *me = mon.find_monsterentry();
  if (me) {
      monster_info mi(&mon, MILEV_NAME);
      symbol.first = me->basechar;
      symbol.second = colour_to_string(mi.colour());
  }
  return (symbol);
}

int mi_create_monster(mons_spec spec) {
    item_list items = spec.items;
    for (unsigned int i = 0; i < spec.items.size(); i++)
    {
        int ego = spec.items.get_item(i).ego;
        if (ego >= 0)
            continue;
        // XXX: this is an unholy hack; xref set_unique_item_status
        item_def tempitem;
        tempitem.flags |= ISFLAG_UNRANDART;
        tempitem.special = -ego;
        set_unique_item_status(tempitem, UNIQ_NOT_EXISTS);
    }
    monster *monster =
        dgn_place_monster(spec, MONSTER_PLACE, true, false, false);
    if (monster) {
        monster->behaviour = BEH_SEEK;
        monster->foe = MHITYOU;
        no_messages mx;
        monster->del_ench(ENCH_SUBMERGED);
        return monster->mindex();
    }
    return NON_MONSTER;
}

static std::string damage_flavour(const std::string &name,
                                  const std::string &damage)
{
  return name + ": " + damage;
}

static std::string damage_flavour(const std::string &name,
                                  int low, int high)
{
  std::stringstream ss;
  ss << name << ": " << low << "-" << high;
  return ss.str();
}

static void rebind_mspec(std::string *requested_name,
                         const std::string &actual_name,
                         mons_spec *mspec)
{
  if (*requested_name != actual_name
      && requested_name->find("draconian") == 0)
  {
    // If the user requested a drac, the game might generate a
    // coloured drac in response. Try to reuse that colour for further
    // tests.
    mons_list mons;
    const std::string err = mons.add_mons(actual_name, false);
    if (err.empty())
    {
      *mspec          = mons.get_monster(0);
      *requested_name = actual_name;
    }
  }
}

static std::string canned_reports[][2] = {
  { "cang",
    ("cang (" + colour(LIGHTRED, "Ω")
     + (") | Spd: c | HD: i | HP: 666 | AC/EV: e/π | Dam: 999"
	 " | Res: sanity | XP: ∞ | Int: god | Sz: !!!")) },
};

void do_holiness(const monsterentry *me) {
    std::string holiness("Natural");
    switch (me->holiness)
    {
    case MH_HOLY:
      holiness = "Holy";
      break;
    case MH_UNDEAD:
      holiness = "Undead";
      break;
    case MH_DEMONIC:
      holiness = "Demonic";
      break;
    case MH_NONLIVING:
      holiness = "Nonliving";
      break;
    case MH_PLANT:
      holiness = "Plant";
      break;
    case MH_NATURAL:
    default:
      break;
    }
    printf("|holiness={{%s}}\n", holiness.c_str());
}

void do_habitat(const monsterentry *me) {
    std::string habitat("Land");
    switch (me->habitat)
    {
    case HT_LAND:
      habitat = "Land";
      break;
    case HT_AMPHIBIOUS:
      habitat = "Amphibious";
      break;
    case HT_WATER:
      habitat = "Water";
      break;
    case HT_LAVA:
      habitat = "Lava";
      break;
    case HT_AMPHIBIOUS_LAVA:
      habitat = "Amphibious Lava";
      break;
    default:
      break;
    }
    printf("|habitat=%s\n", habitat.c_str());
}


static void do_item_use(const monsterentry *me, const monster &mon) {
    std::string itemuse("|item_use=");
    switch (me->gmon_use)
    {
      case MONUSE_WEAPONS_ARMOUR:
        itemuse += "{{Weapons armour}}<br>";
      // intentional fall-through
      case MONUSE_STARTING_EQUIPMENT:
        itemuse += "{{Starting equipment}}<br>";
      // intentional fall-through
      case MONUSE_OPEN_DOORS:
        itemuse += "{{Open doors}}";
        break;
      case MONUSE_NOTHING:
        itemuse += "{{Uses nothing}}";
        break;

      case NUM_MONUSE:  // Can't happen
        break;
    }
    printf("%s\n", itemuse.c_str());
}

static void do_flags(const monsterentry *me, monster &mon, const bool shapeshifter, const bool vault_monster, const std::string spell_abilities) {
    std::string monsterflags("");
    std::string monsterflagsfull("|flags=");

    switch (me->gmon_eat)
    {
      case MONEAT_NOTHING:
        break;
      case MONEAT_ITEMS:
        mons_flag(monsterflags, "{{Eats items}}");
        break;
      case MONEAT_CORPSES:
        mons_flag(monsterflags, "{{Eats corpses}}");
        break;
      case MONEAT_FOOD:
        mons_flag(monsterflags, "{{Eats food}}");
        break;
      case MONEAT_DOORS:
        mons_flag(monsterflags, "{{Eats doors}}");
        break;
      case NUM_MONEAT:  // Can't happen
        mons_flag(monsterflags, "{{eats bugs}}");
        break;
    }

    mons_check_flag(me->bitfields & M_ACID_SPLASH, monsterflags, "{{Acid splash flag}}");
    mons_check_flag(me->bitfields & M_ALWAYS_CORPSE, monsterflags, "{{Always corpse flag}}");
    mons_check_flag(me->bitfields & M_ARCHER, monsterflags, "{{Archer flag}}");
    mons_check_flag(me->bitfields & M_ARTIFICIAL, monsterflags, "{{Artificial flag}}");
    mons_check_flag(me->bitfields & M_BATTY, monsterflags, "{{Batty flag}}");
    mons_check_flag(me->bitfields & M_BLOOD_SCENT, monsterflags, "{{Blood scent flag}}");
    mons_check_flag(me->bitfields & M_BURROWS, monsterflags, "{{Burrows flag}}");
    mons_check_flag(me->bitfields & M_CONFUSED, monsterflags, "{{Confused flag}}");
    mons_check_flag(me->bitfields & M_COLD_BLOOD, monsterflags, "{{Cold blood flag}}");
    mons_check_flag(mon.is_evil(), monsterflags, "{{Evil flag}}");
    mons_check_flag(me->bitfields & M_FIGHTER, monsterflags, "{{Fighter flag}}");
    mons_check_flag(me->bitfields & M_FLEES, monsterflags, "{{Flees flag}}");
    mons_check_flag(me->fly == FL_WINGED, monsterflags, "{{Flying flag}}");
    mons_check_flag(me->bitfields & (M_GLOWS_LIGHT|M_GLOWS_RADIATION), monsterflags, "{{Glows flag}}");
    if (mon.type == MONS_MINOTAUR)
        mons_flag(monsterflags, "{{Headbutt flag}}");
    mons_check_flag(me->bitfields & M_HERD, monsterflags, "{{Herd flag}}");
    mons_check_flag(me->bitfields & M_HYBRID, monsterflags, "{{Hybrid flag}}");
    mons_check_flag(me->bitfields & M_INSUBSTANTIAL, monsterflags, "{{Insubstantial flag}}");
    mons_check_flag(me->fly == FL_LEVITATE, monsterflags, "{{Levitate flag}}");
    mons_check_flag(me->bitfields & M_MAINTAIN_RANGE, monsterflags, "{{Maintain range flag}}");
    mons_check_flag(me->bitfields & M_NO_HT_WAND, monsterflags, "{{No HT wand flag}}");
    mons_check_flag(me->bitfields & M_NO_REGEN, monsterflags, "{{No regenerate flag}}");
    if (!mons_can_use_stairs(&mon))
      mons_flag(monsterflags, "{{No stairs flag}}");

    mons_check_flag(me->bitfields & M_NO_SKELETON, monsterflags, "{{No skeleton flag}}");
    mons_check_flag(me->bitfields & M_NO_EXP_GAIN, monsterflags, "{{No exp gain flag}}");
    mons_check_flag(me->bitfields & M_NO_GEN_DERIVED, monsterflags, "{{No gen derived flag}}");
    mons_check_flag(me->bitfields & M_NO_POLY_TO, monsterflags, "{{No poly flag}}");
    mons_check_flag(me->bitfields & M_NO_ZOMBIE, monsterflags, "{{No zombie flag}}");
    mons_check_flag(me->bitfields & M_PHASE_SHIFT, monsterflags, "{{Phase shift flag}}");
    mons_check_flag(me->bitfields & M_FAST_REGEN, monsterflags, "{{Regenerates flag}}");

    mons_check_flag(me->bitfields & M_SEE_INVIS, monsterflags, "{{See invisible flag}}");
    mons_check_flag(me->bitfields & M_SPEAKS, monsterflags, "{{Speaks flag}}");
    if (mon.spiny_degree() > 0)
        mons_flag(monsterflags, "{{Spiny flag}}");
    mons_check_flag(me->bitfields & M_SPLITS, monsterflags, "{{Splits flag}}");
    mons_check_flag(me->bitfields & M_STATIONARY, monsterflags, "{{Stationary flag}}");
    mons_check_flag(me->bitfields & M_SUBMERGES, monsterflags, "{{Submerges flag}}");
    mons_check_flag(me->bitfields & M_UNIQUE, monsterflags, "{{Unique flag}}");
    mons_check_flag(me->bitfields & M_TWO_WEAPONS, monsterflags, "{{Two weapon flag}}");
    mons_check_flag(me->bitfields & M_UNBREATHING, monsterflags, "{{Unbreathing flag}}");
    mons_check_flag(vault_monster, monsterflags, "{{Vault flag}}");
    mons_check_flag(me->bitfields & M_WARM_BLOOD, monsterflags, "{{Warm blood flag}}");
    mons_check_flag(me->bitfields & M_WEB_SENSE, monsterflags, "{{Web sense flag}}");
#if 0
    mons_check_flag(!spell_abilities.empty()
                    && !mon.is_priest() && !mon.is_actual_spellcaster(),
                    monsterflags, "{{!sil flag}}");
#endif
    printf("%s%s\n", monsterflagsfull.c_str(), monsterflags.c_str());
}

static void do_attacks(const monsterentry *me, monster &mon) {
    std::string monsterattacks;
    mon.wield_melee_weapon();
    for (int x = 0; x < 4; x++)
    {
      mon_attack_def orig_attk(me->attack[x]);
      mon_attack_def attk = mons_attack_spec(&mon, x);

      monsterattacks += "|attack" + to_string(x + 1) + "=";
      
      if (attk.type)
      {
        monsterattacks += to_string((short int) attk.damage) + " (";

	switch (attk.type)
	{
	case AT_BITE:
	    monsterattacks += "{{Bite type}}";
	    break;
	case AT_CHERUB:
	    monsterattacks += "{{Cherub type}}";
	    break;
	case AT_CLAW:
	    monsterattacks += "{{Claw type}}";
	    break;
	case AT_CONSTRICT:
	    monsterattacks += "{{Constrict type}}";
	    break;
	case AT_GORE:
	    monsterattacks += "{{Gore type}}";
	    break;
	case AT_HIT:
	    monsterattacks += "{{Hit type}}";
	    break;
	case AT_KICK:
	    monsterattacks += "{{Kick type}}";
	    break;
	case AT_PECK:
	    monsterattacks += "{{Peck type}}";
	    break;
	case AT_RANDOM:
	    monsterattacks += "{{Random hit type}}";
	    break;
	case AT_SHOOT:
	    monsterattacks += "{{Shoot type}}";
	    break;
	case AT_SPORE:
	    monsterattacks += "{{Spore type}}";
	    break;
	case AT_STING:
	    monsterattacks += "{{Sting type}}";
	    break;
	case AT_TAIL_SLAP:
	    monsterattacks += "{{Tail slap type}}";
	    break;
	case AT_TENTACLE_SLAP:
	    monsterattacks += "{{Tentacle slap type}}";
	    break;
	case AT_TOUCH:
	    monsterattacks += "{{Touch type}}";
	    break;
	case AT_TRAMPLE:
	    monsterattacks += "{{Trample type}}";
	    break;
	case AT_TRUNK_SLAP:
	    monsterattacks += "{{Trunk slap type}}";
	    break;
	case AT_WEAP_ONLY:
	    monsterattacks += "{{Weapon only type}}";
	    break;
	case AT_ENGULF:
	    monsterattacks += "{{Engulf type}}";
	    break;
	case AT_POUNCE:
	    monsterattacks += "{{Pounce type}}";
	    break;
        // Unused
	case AT_SNAP:
	    monsterattacks += "{{Snap type}}";
	    break;
	case AT_SPLASH:
	    monsterattacks += "{{Splash type}}";
	    break;
	case AT_PUNCH:
	    monsterattacks += "{{Punch type}}";
	    break;
	case AT_HEADBUTT:
	    monsterattacks += "{{Headbutt type}}";
	    break;
	case AT_REACH_STING:
	    monsterattacks += "{{Reach sting type}}";
	    break;
        case AT_NONE:
            break;
	}
	monsterattacks += ": ";

        const attack_flavour flavour(
            orig_attk.flavour == AF_KLOWN ? AF_KLOWN
#if TAG_MAJOR_VERSION == 32
          : orig_attk.flavour == AF_SUBTRACTOR ? AF_SUBTRACTOR
#endif
          : attk.flavour);

        switch (flavour)
        {
        case AF_REACH:
          monsterattacks += "{{Reach flavour}}";
          break;
        case AF_ACID:
          monsterattacks += damage_flavour("{{Acid flavour}}", "7d3");
          break;
        case AF_BLINK:
          monsterattacks += "{{Blink flavour}}";
          break;
        case AF_COLD:
          monsterattacks += damage_flavour("{{Cold flavour}}", mon.get_hit_dice(),
                                           3 * mon.get_hit_dice() - 1);
          break;
        case AF_CONFUSE:
          monsterattacks += "{{Confuse flavour}}";
          break;
        case AF_DISEASE:
          monsterattacks += "{{Disease flavour}}";
          break;
        case AF_DRAIN_DEX:
          monsterattacks += "{{Drain dexterity flavour}}";
          break;
        case AF_DRAIN_STR:
          monsterattacks += "{{Drain strength flavour}}";
          break;
        case AF_DRAIN_XP:
          monsterattacks += "{{Drain experience flavour}}";
          break;
        case AF_CHAOS:
          monsterattacks += "{{Chaos flavour}}";
          break;
        case AF_ELEC:
          monsterattacks +=
            damage_flavour("{{Electricity flavour}}", mon.get_hit_dice(),
                           mon.get_hit_dice() +
                           std::max(mon.get_hit_dice() / 2 - 1, 0));
          break;
        case AF_FIRE:
          monsterattacks +=
            damage_flavour("{{Fire flavour}}", mon.get_hit_dice(),
                           mon.get_hit_dice() * 2 - 1);
          break;
        case AF_STICKY_FLAME:
          monsterattacks += "{{Sticky flame flavour}}";
          break;
        case AF_HUNGER:
          monsterattacks += "{{Hunger flavour}}";
          break;
        case AF_MUTATE:
          monsterattacks += "{{Mutate flavour}}";
          break;
        case AF_PARALYSE:
          monsterattacks += "{{Paralyse flavour}}";
          break;
        case AF_POISON:
          monsterattacks += damage_flavour("{{Poison flavour}}",
                                           mon.get_hit_dice() * 2,
                                           mon.get_hit_dice() * 4);
          break;
        case AF_POISON_STRONG:
          monsterattacks += damage_flavour("{{Strong poison flavour}}",
                                           mon.get_hit_dice() * 11 / 3,
                                           mon.get_hit_dice() * 13 / 2);
          break;
        case AF_ROT:
          monsterattacks += "{{Rot flavour}}";
          break;
        case AF_VAMPIRIC:
          monsterattacks += "{{Vampiric flavour}}";
          break;
        case AF_KLOWN:
          monsterattacks += "{{Klown flavour}}";
          break;
#if TAG_MAJOR_VERSION == 32
        case AF_SUBTRACTOR:
          monsterattacks += "(subtractor)";
          break;
#endif
        case AF_DISTORT:
          monsterattacks += "{{Distort flavour}}";
          break;
        case AF_RAGE:
          monsterattacks += "{{Rage flavour}}";
          break;
        case AF_HOLY:
          monsterattacks += "{{holy flavour}}";
          break;
        case AF_PAIN:
          monsterattacks += "{{Pain flavour}}";
          break;
        case AF_ANTIMAGIC:
          monsterattacks += "{{Antimagic flavour}}";
          break;
        case AF_DRAIN_INT:
          monsterattacks += "{{Drain intelligence flavour}}";
          break;
        case AF_DRAIN_STAT:
          monsterattacks += "{{Drain stat flavour}}";
          break;
        case AF_STEAL:
          monsterattacks += "{{Steal flavour}}";
          break;
        case AF_ENSNARE:
          monsterattacks += "{{Ensnare flavour}}";
          break;
        case AF_CRUSH:
          monsterattacks += "{{Crush flavour}}";
	  break;
        case AF_DROWN:
          monsterattacks += "{{Drown flavour}}";
	  break;
        case AF_PURE_FIRE:
          monsterattacks += damage_flavour("{{Pure fire flavour}}", 3 * mon.get_hit_dice() / 2,
                                           3 * mon.get_hit_dice() / 2 + mon.get_hit_dice() - 1);
	  break;
        case AF_DRAIN_SPEED:
          monsterattacks += "{{Drain speed flavour}}";
	  break;
        case AF_VULN:
          monsterattacks += "{{Vuln flavour}}";
	  break;
        case AF_PLAGUE:
          monsterattacks += "{{Plague flavour}}";
	  break;
        case AF_WEAKNESS_POISON:
          monsterattacks += "{{Weakness poison flavour}}";
	  break;
        case AF_SHADOWSTAB:
          monsterattacks += "{{Shadow stab flavour}}";
	  break;
        case AF_PLAIN:
          monsterattacks += "{{Plain flavour}}";
          break;
        case AF_FIREBRAND:
          monsterattacks += damage_flavour("{{Firebrand flavour}}",
                                           mon.get_hit_dice(),
                                           mon.get_hit_dice() * 2 - 1);
          break;
        case AF_ENGULF:
          monsterattacks += "{{Engulf flavour}}";
          break;
        case AF_STEAL_FOOD:
        case AF_POISON_NASTY:
        case AF_POISON_MEDIUM:
        case AF_POISON_STR:
        case AF_POISON_INT:
        case AF_POISON_DEX:
        case AF_POISON_STAT:
          monsterattacks += "{{Obsolete attack flavour}}";
          break;
        case AF_CORRODE:
            monsterattacks += "{{Corrode flavour}}";
            break;
        case AF_SCARAB:
            monsterattacks += "{{Scarab flavour}}";
            break;
        case AF_KITE:
            monsterattacks += "{{Kite flavour}}";
            break;
        case AF_TRAMPLE:
            monsterattacks += "{{Trample flavour}}";
            break;
        case AF_SWOOP:
            monsterattacks += "{{Swoop flavour}}";
            break;

// let the compiler issue warnings for us
//      default:
//        monsterattacks += "(???)";
//        break;
        }
	monsterattacks += ")";

        if (mon.has_hydra_multi_attack())
          monsterattacks += " per head";
      }
      monsterattacks += "\n";
      if (mon.has_hydra_multi_attack())
      {
	monsterattacks += "|attack2=\n|attack3=\n|attack4=\n";
        break;
      }
    }

    printf("%s", monsterattacks.c_str());
}

void do_speed(const monsterentry *me, const monster &mon, int speed_min, int speed_max) {
    printf("|speed= %s\n",
           monster_speed(mon, me, speed_min, speed_max).c_str());
}

void do_meat(const monsterentry *me, const monster &mon) {
    std::string meat("|meat=");
    int max_chunks = 0;
    monster_type sp = me->species;
    monsterentry* me_sp = get_monster_data(sp);

    if (me_sp->weight != 0)
    {
        max_chunks = get_max_corpse_chunks(sp);
        max_chunks = stepdown_value(max_chunks, 4, 4, 12, 12);
        switch (me_sp->corpse_thingy)
	  {
	  case CE_POISONOUS:
	    meat += "{{Poisonous corpse}}";
	    break;
	  case CE_ROT:
	    meat += "{{Rot-inducing corpse}}";
	    break;
	  case CE_MUTAGEN:
	    meat += "{{Mutagenic corpse}}";
	    break;
	  case CE_CLEAN:
	    meat += "{{Clean corpse}}";
	    break;
	  // We should't get here; including these values so we can get compiler
	  // warnings for unhandled enum values.
	  case CE_NOCORPSE:
	    meat += "???";
	  }
    }
    else
      meat += "{{No corpse}}";
    printf("|max_chunks=%d\n", max_chunks);
    printf("%s\n", meat.c_str());
}

void do_magic_resistance(const monsterentry *me, const monster &mon, monster_type const &spec_type) {
    std::string result("|magic_resistance=");
    int mr = me->resist_magic;
    if (mr == MAG_IMMUNE)
      result += "Immune";
    else if (mr < 0)
      result += to_string((short int) mons_class_hit_dice(spec_type) * mr * 4 / 3 * -1);
    else if (mr >= 0)
      result += to_string((short int) mr);

    printf("%s\n", result.c_str());
}

// See art-func.h:is_dragonkind
static bool is_dragonkind(const actor *act)
{
    return mons_genus(act->mons_species()) == MONS_DRAGON
        || mons_genus(act->mons_species()) == MONS_DRAKE
        || mons_genus(act->mons_species()) == MONS_DRACONIAN;
}

void do_resistances_vulnerabilities(const monsterentry *me, const monster &mon, bool shapeshifter) {
    std::string monsterresistances;
    std::string monstervulnerabilities;
    const resists_t res(
      shapeshifter? me->resists : get_mons_resists(&mon));
#define res(r,x)                                  \
    do                                            \
    {                                             \
      record_resist(r,                            \
                    monsterresistances,           \
                    monstervulnerabilities,       \
                    get_resist(res, MR_RES_##x)); \
    } while (false)                               \

#define res2(x,y)                                 \
    do                                            \
    {                                             \
      record_resist(#x,                           \
                    monsterresistances,           \
                    monstervulnerabilities,       \
                    y);                           \
    } while (false)                               \


    // Don't record regular rF as hellfire vulnerability.
    bool rhellfire = get_resist(res, MR_RES_FIRE) >= 4;
    res2(Hellfire, (int)rhellfire);
    if (!rhellfire)
      res("Fire", FIRE);
    res("Cold", COLD);
    res("Electricity", ELEC);
    res("Poison", POISON);

    res2(Negative energy,    mon.res_negative_energy());
    res2(Torment,   mon.res_torment());
    res2(Rot,    mon.res_rotting());

    res2(Sticky flame, mon.res_sticky_flame());

    res("Acid", ACID);
    res2(Wind,   mon.res_wind());
    res2(Constriction,   (mon.res_constrict()==3?1:0));
    res2(Drown,  (monster_habitable_grid(&mon, DNGN_DEEP_WATER) || !mon.can_drown()));
    res2(Water,  mon.res_water_drowning());
    res("Steam", STEAM);
    res("Asphyxiation", ASPHYX);

    res2(Holy,   mon.res_holy_energy(&you));
    res2(Holy wrath, mon.undead_or_demonic()?-1:0);
    res2(Silver,   (mon.how_chaotic()?-1:0));
    res2(Dragon slaying,   (is_dragonkind(&mon)?-1:0));

    if (monsterresistances.empty())
      monsterresistances = "None";
    if (monstervulnerabilities.empty())
      monstervulnerabilities = "None";

    printf("|resistances=%s\n", monsterresistances.c_str());
    printf("|vulnerabilities=%s\n", monstervulnerabilities.c_str());

}

void do_xp(long exper) {
    printf("|xp=%ld\n", exper);
}

void do_hd(const monsterentry *me, const monster &mon) {
  const int hd = me->hpdice[0];
    printf("|hit_dice=%d\n", hd);
    printf("|base_hp=%d\n", me->hpdice[1]);
    printf("|extra_hp=%d\n", me->hpdice[2]);
    printf("|fixed_hp=%d\n", me->hpdice[3]);
}

void do_hp(const int hp_min, const int hp_max) {
    printf("|hp_range=");
    const int hplow = hp_min;
    const int hphigh = hp_max;
    if (hplow < hphigh)
    {
        printf("%i-%i\n", hplow, hphigh);
        printf("|avg_hp=%i\n", (hphigh+hplow)/2);
    }
    else
    {
        printf("%i\n", hplow);
        printf("|avg_hp=%i\n", hplow);
    }
}

void do_ac(const monsterentry *me, const bool generated, const int mac) {
    const int ac = generated? mac : me->AC;
    printf("|armour_class=%i\n", ac);
}

void do_ev(const monsterentry *me, const bool generated, const int mev) {
    const int ev = generated? mev : me->ev;
    printf("|evasion=%i\n", ev);
}

void do_spells(const std::string spell_abilities) {
    if (!spell_abilities.empty())
      printf("|speed=%s\n", spell_abilities.c_str());
}

void do_size(const monster &mon) {
    printf("|size={{%s}}\n",
           monster_size(mon).c_str());
}

void do_intelligence(const monster &mon) {
    printf("|intelligence=%s\n",
           monster_int(mon).c_str());
}

void do_genus(std::string genus) {
    printf("|genus=%s\n", genus.c_str());
}

void do_species(std::string species) {
    printf("|species=%s\n", species.c_str());
}

string filter_info(string const& desc) {
    std::string::size_type end = desc.rfind("\nAC ", desc.size()-1);
    if (end == std::string::npos)
        return desc;
    return desc.substr(0, end - 1);
}

// Removes indentation and unicode quote characters
void clean_up_quote(std::string& quote) {
  std::string::size_type pos;
  // remove whitespaces after newlines
  pos = 0;
  while ( (pos = quote.find("\n", pos)) != std::string::npos) {
    pos++;
    if (pos >= quote.size())
      break;
    std::string::size_type blank = pos;
    while (blank < quote.size() && quote[blank] == ' ')
      blank++;
    if (blank != pos)
      quote.erase(pos, blank-pos);
  }
  //remove newlines at the end
  pos = quote.size() - 1;
  int spaces = 0;
  while (pos >= 0 && quote[pos] == '\n') {
      pos--;
      spaces++;
  }
  if (spaces > 0)
    quote.erase(quote.size() - spaces);

  //remove whitespaces at the beginning
  pos = 0;
  while ( pos < quote.size() && quote[pos] == ' ')
    pos++;
  quote.erase(0, pos);
}

std::string insert_line_break(std::string const& s) {
  std::string result;
  std::string::size_type pos = 0;
  while (pos < s.size()) {
    if (s[pos] == '\n')
      result += "<br>";
    result.push_back(s[pos]);
    pos++;
  }
  return result;
}

bool is_shared_quote(const std::string& name) {
  std::string tmp[] = {"merfolk", "merfolk javelineer", "merfolk impaler",
                       "merfolk aquamancer", "mummy", "greater mummy", "mummy priest",
                       "red draconian", "white draconian", "green draconian",
                       "grey draconian", "yellow draconian", "black draconian",
                       "mottled draconian", "pale draconian", "purple draconian",
                       "deep troll", "deep troll shaman", "deep troll earth mage",
                       "troll"
  };
  std::set<std::string> shared_mons(tmp, tmp + sizeof(tmp) / sizeof(tmp[0]));
  if (shared_mons.find(name) != shared_mons.end())
    return true;
  return false;
}

void do_description(const monster& mon, const std::string& name) {
  monster_info mi(&mon);
  describe_info inf;
  bool has_stat_desc = false;
  get_monster_db_desc(mi, inf, has_stat_desc, true);
  std::string body = inf.body.str();
  std::string& quote = inf.quote;
  if (!body.empty()) {
    body = insert_line_break(filter_info(body));
    if (!quote.empty() && !is_shared_quote(name)) {
      clean_up_quote(quote);
      quote = insert_line_break(quote);
      printf("{{Flavour|%s\n----\n%s}}\n", body.c_str(), quote.c_str());
    } else {
      printf("{{Flavour|%s}}\n", filter_info(body).c_str());
    }
  }
}

static void draco_or_demonspawn_adjust(int index)
{
    monster *mp = &menv[index];

    switch (mp->type)
    {
    case MONS_DRACONIAN_CALLER:
    case MONS_DRACONIAN_MONK:
    case MONS_DRACONIAN_ZEALOT:
    case MONS_DRACONIAN_SHIFTER:
    case MONS_DRACONIAN_ANNIHILATOR:
    case MONS_DRACONIAN_SCORCHER:
    case MONS_DRACONIAN_KNIGHT:
    {
        mp->base_monster = MONS_DRACONIAN;
        break;
    }
    case MONS_BLOOD_SAINT:
    case MONS_CHAOS_CHAMPION:
    case MONS_WARMONGER:
    case MONS_CORRUPTER:
    case MONS_BLACK_SUN:
    {
        mp->base_monster = MONS_DEMONSPAWN;
        break;
    }
    default:
        break;
    }
}

bool is_professional_draco_or_demonspawn(monster const & mon)
{
    switch (mon.type)
    {
    case MONS_DRACONIAN_CALLER:
    case MONS_DRACONIAN_MONK:
    case MONS_DRACONIAN_ZEALOT:
    case MONS_DRACONIAN_SHIFTER:
    case MONS_DRACONIAN_ANNIHILATOR:
    case MONS_DRACONIAN_SCORCHER:
    case MONS_DRACONIAN_KNIGHT:
        // fallthrough
    case MONS_BLOOD_SAINT:
    case MONS_CHAOS_CHAMPION:
    case MONS_WARMONGER:
    case MONS_CORRUPTER:
    case MONS_BLACK_SUN:
    {
        return true;
    }
    default:
        return false;
    }
}


int main(int argc, char *argv[])
{
  crawl_state.test = true;
  if (argc < 2)
  {
    printf("Usage: @? <monster name>\n");
    return 0;
  }

  if (strstr(argv[1], "-version"))
  {
    printf("Monster stats Crawl version: %s\n", Version::Long);
    return 0;
  }

  bool spells = false;
  size_t argi = 1;
  if (strstr(argv[argi], "-spell"))
  {
    if (argc < 3)
    {
      printf("Usage: @? <monster name>\n");
      return 0;
    }
    else
    {
      spells = true;
      argi++;
    }
  }

  initialize_crawl();

  mons_list mons;
  std::string target = argv[argi++];

  if (argc > 2)
    for (int x = argi; x < argc; x++)
    {
      target.append(" ");
      target.append(argv[x]);
    }

  trim_string(target);

  const bool want_vault_spec = target.find("spec:") == 0;
  if (want_vault_spec)
  {
      target.erase(0, 5);
      trim_string(target);
  }

  // [ds] Nobody mess with cang.
  for (unsigned i = 0; i < sizeof(canned_reports) / sizeof(*canned_reports);
       ++i)
  {
    if (canned_reports[i][0] == target)
    {
      printf("%s\n", canned_reports[i][1].c_str());
      return 0;
    }
  }

  std::string orig_target = std::string(target);

  std::string err = mons.add_mons(target, false);
  if (!err.empty()) {
    target = "the " + target;
    const std::string test = mons.add_mons(target, false);
    if (test.empty())
      err = test;
  }

  mons_spec spec = mons.get_monster(0);
  monster_type spec_type = static_cast<monster_type>(spec.type);
  bool vault_monster = false;
  string vault_spec;

  if ((spec_type < 0 || spec_type >= NUM_MONSTERS
       || spec_type == MONS_PLAYER_GHOST)
      || !err.empty())
  {
      spec = get_vault_monster(orig_target, &vault_spec);
    spec_type = static_cast<monster_type>(spec.type);
    if (spec_type < 0 || spec_type >= NUM_MONSTERS
        || spec_type == MONS_PLAYER_GHOST)
    {
      if (err.empty())
        printf("unknown monster: \"%s\"\n", target.c_str());
      else
        printf("%s\n", err.c_str());
      return 1;
    }
    // get_vault_monster created the monster; make uniques ungenerated again
    if (mons_is_unique(spec_type))
      you.unique_creatures.set(spec_type, false);

    vault_monster = true;
  }

  if (want_vault_spec)
  {
    if (!vault_monster)
    {
      printf("Not a vault monster: %s\n", orig_target.c_str());
      return 1;
    }
    else
    {
      printf("%s: %s\n", orig_target.c_str(), vault_spec.c_str());
      return 0;
    }
  }

  int index = mi_create_monster(spec);
  if (index < 0 || index >= MAX_MONSTERS) {
    printf("Failed to create test monster for %s\n", target.c_str());
    return 1;
  }
  draco_or_demonspawn_adjust(index);

  const int ntrials = 1000;

  std::set<std::string> spell_sets;
  std::set<std::string> spell_sets2;

  long exper = 0L;
  int hp_min = 0;
  int hp_max = 0;
  int mac = 0;
  int mev = 0;
  int speed_min = 0, speed_max = 0;
  // Calculate averages.
  for (int i = 0; i < ntrials; ++i) {
    if (i == ntrials)
      break;
    monster *mp = &menv[index];
    const std::string mname = mp->name(DESC_PLAIN, true);
    if (!mons_class_is_zombified(mp->type))
    {
        // record_spell_set(mp, spell_sets);
      record_spell_set2(mp, spell_sets2);
    }
    exper += exper_value(mp);
    mac += mp->base_armour_class();
    mev += mp->base_evasion();
    set_min_max(mp->speed, speed_min, speed_max);
    set_min_max(mp->hit_points, hp_min, hp_max);

    // Destroy the monster.
    mp->reset();
    you.unique_creatures.set(spec_type, false);

    rebind_mspec(&target, mname, &spec);

    index = mi_create_monster(spec);
    draco_or_demonspawn_adjust(index);
    if (index == -1) {
      printf("Unexpected failure generating monster for %s\n",
             target.c_str());
      return 1;
    }
  }
  exper /= ntrials;
  mac /= ntrials;
  mev /= ntrials;

  // Destroy the monster.
  monster *mp = &menv[index];
  const std::string mname = mp->name(DESC_PLAIN, true);
  mp->reset();
  you.unique_creatures.set(spec_type, false);
  
  rebind_mspec(&target, mname, &spec);

  index = mi_create_monster(spec);

  if (index == -1) {
      printf("Unexpected failure generating monster for %s\n",
             target.c_str());
      return 1;
  }

  monster &mon(menv[index]);

  const symbol symb(monster_symbol(mon));
  const string symb_first(symb.first);
  const string symb_second(symb.second);
  
  // adjust after we have got the correct symbol
  draco_or_demonspawn_adjust(index);

  const bool generated =
    mons_class_is_zombified(mon.type)
    || mon.type == MONS_HELL_BEAST || mon.type == MONS_PANDEMONIUM_LORD
    || mon.type == MONS_UGLY_THING || mon.type == MONS_DANCING_WEAPON;

  const bool shapeshifter =
      mon.is_shapeshifter()
      || spec_type == MONS_SHAPESHIFTER
      || spec_type == MONS_GLOWING_SHAPESHIFTER;

  const bool is_professional = is_professional_draco_or_demonspawn(mon);
  
  const bool lost_soul = spec_type == MONS_LOST_SOUL;
  const std::string spell_abilities =
    mons_spells_abilities(&mon, shapeshifter, spell_sets);

  const monsterentry *me =
      shapeshifter||lost_soul ? get_monster_data(spec_type) : mon.find_monsterentry();
  const monster_type genus = me->genus;
  const monsterentry *genus_entry = get_monster_data(genus);
  const std::string genus_name = genus_entry->name;
  const monster_type species = me->species;
  const monsterentry *species_entry = get_monster_data(species);
  const std::string species_name = species_entry->name;

  if (me)
  {
    std::string monsterflags;

    lowercase(target);

    const bool changing_name =
      mon.has_hydra_multi_attack() || mon.type == MONS_PANDEMONIUM_LORD
        || shapeshifter
        || mon.type == MONS_DANCING_WEAPON;
    std::string name = changing_name || is_professional ? me->name : mon.name(DESC_PLAIN, true).c_str();
    std::string cap_name(1, ::toupper(name[0]));
    cap_name += name.substr(1);

    if (spells)
    {
      const std::string spell_abilities =
	mons_spells_abilities2(&mon, cap_name, shapeshifter, spell_sets2);
      if (!spell_abilities.empty())
      {
	  printf("%s", spell_abilities.c_str());
      }
      return 0;
    }
    else
    {
        printf("{{monster\n");
	printf("|name=%s\n", name.c_str());
	printf("|glyph={{%s|%s}}\n", symb_second.c_str(), symb_first.c_str());
	printf("|tile=[[File:%s.png]]\n", cap_name.c_str());

#if 0
	if (mons_class_flag(mon.type, M_UNFINISHED))
	  printf(" | %s", colour(LIGHTRED, "UNFINISHED").c_str());
#endif

	do_flags(me, mon, shapeshifter, vault_monster, spell_abilities);
	do_resistances_vulnerabilities(me, mon, shapeshifter);
	do_meat(me, mon);
	do_xp(exper);
	do_holiness(me);
	do_magic_resistance(me, mon, spec_type);
	do_hp(hp_min, hp_max);
	//do_avg_hp
	do_ac(me, generated, mac);
	do_ev(me, generated, mev);
	do_habitat(me);
	do_speed(me, mon, speed_min, speed_max);
	do_size(mon);
	do_item_use(me, mon);
	do_attacks(me, mon);
	do_hd(me, mon);
	// do_extra_hp
	do_intelligence(mon);
	do_genus(genus_name);
	do_species(species_name);
	// do_species


	printf("}}\n");
        do_description(mon, name);
    }

    return 0;
  }
  return 1;
}

template <class T> inline std::string to_string (const T& t)
{
  std::stringstream ss;
  ss << t;
  return ss.str();
}

//////////////////////////////////////////////////////////////////////////
// acr.cc stuff

CLua clua(true);
CLua dlua(false);      // Lua interpreter for the dungeon builder.
crawl_environment env; // Requires dlua.
player you;
game_state crawl_state;

FILE *yyin;
int yylineno;

std::string init_file_error;    // externed in newgame.cc

int stealth;                    // externed in view.cc
bool apply_berserk_penalty;     // externed in evoke.cc

void process_command(command_type) {
}

int yyparse() {
  return 0;
}

void world_reacts() {
}
