#!/usr/bin/perl
use strict;
use warnings;
use English;
use Encode qw(encode decode);
use Carp;
use MediaWiki::API;
use File::Spec;
use IO::Dir;
use Getopt::Long;

$ENV{'LANG'} = 'en_US.UTF-8';
# Mode of operation: monster-trunk, monster-tile, monster-glyph
my $mode = 'monster-trunk';
# The location of the monster-trunk executable
my $monster_trunk = "./monster-trunk";
my $monster_tile = "./monster-tile";
# List the wiki PHP scripts where you have the username/password pair
my $api_url = 'http://crawl.chaosforge.org/api.php';
my $upload_url = 'http://crawl.chaosforge.org/Special:Upload';
# Set the pause in seconds after each upload
my $pause = 0;
# $debug == 0 normal operation
# $debug == 1 edit $debug_page
# $debug == 2 edit $debug_page with log messages
# $debug == 3  print changes that would be done
# $debug == 4  print changes that would be done to $debug_page
my $debug = 0;
my $debug_page = 'Insubstantial wisp';
my $default_summary = 'Update for 0.15';
my $only_spells = 0;
# If $skip is true, we won't process any articles until $skip_page is processed
my $skip = 0;
my $skip_page = 'Bat';
my $ignore_warnings = 0;
my $username = "Username";
my $password = "Password";

GetOptions(
           "mode=s" => \$mode,
           "monster-trunk=s" => \$monster_trunk,
           "monster-tile=s" => \$monster_tile,
           "pause=i" => \$pause,
           "debug=i" => \$debug,
           "debug-page=s" => \$debug_page,
           "default-summary=s" => \$default_summary,
           "only-spells" => \$only_spells,
           "skip" => \$skip,
           "skip-page=s" => \$skip_page,
           "ignore-warnings" => \$ignore_warnings,
           "username=s" => \$username,
           "password=s" => \$password
) or die ("Invalid arguments\n");

my $mw = MediaWiki::API->new( {
    api_url => $api_url,
    upload_url => $upload_url,
    on_error => \&on_error
} );  
my $r = $mw->login( { lgname => $username, lgpassword => $password } );
die("Login failed\n") unless defined($r);

do_monster_trunk() if ($mode eq 'monster-trunk');
do_monster_tile() if ($mode eq 'monster-tile');
do_monster_glyph() if ($mode eq 'monster-glyph');

# quit
$mw->logout();

sub on_error {
  print "Error code: " . $mw->{error}->{code} . "\n";
  print $mw->{error}->{stacktrace}."\n";
  die;
}

sub get_category_members {
    my $category = shift;
    return $mw->list ( {
                        action => 'query',
                        list => 'categorymembers',
                        cmtitle => $category,
                        cmlimit => 'max' } );
}

sub print_articles {
    my $articles = shift;
    foreach (@{$articles}) {
        print "$_->{title}\n";
    }
}

sub contains {
    my ($el, $arr) = @_;
    for (@$arr) {
        if ($el eq $_) {return 1;}
    }
    return 0;
}

sub filter_articles_array {
    my ($articles, $arr) = @_;
    my $r = [];
    foreach (@{$articles}) {
        my $title = $_->{title};
        if (contains($title, \@$arr)) {next;}
        push @$r, $_;
    }
    return $r;
}

sub filter_articles_category {
    my ($articles, $category) = @_;
    my $category_result = get_category_members($category);
    my @arr = map {$_->{title}} @$category_result;
    return filter_articles_array($articles, \@arr);
}

sub filter_articles_string {
    my ($articles, $needle) = @_;
    my $r = [];
    foreach (@{$articles}) {
        my $title = $_->{title};
        if (index($title, $needle) != -1) {next;}
        push @$r, $_;
    }
    return $r;
}

sub filter_articles_obsolete_categories {
    my $articles = shift;
    my $r1 = filter_articles_category($articles, 'Category:Obsolete');
    my $r2 = filter_articles_category($r1, 'Category:Obsolete monsters');
    my $r3 = filter_articles_category($r2, 'Category:Obsolete articles');
    my $r4 = filter_articles_category($r3, 'Category:Monsters no bot');
    return $r4;
}

sub filter_articles_monster_trunk {
    my $articles = shift;
    my $r1 = filter_articles_string($articles, 'Category');
    my $r2 = filter_articles_obsolete_categories($r1);
    my $r3 = filter_articles_array($r2,
                                   ['Out-of-depth', 'List of monsters', 'Monster generation',
                                    'Monsters', 'Lightning spire', 'User:CommanderC/Test','Monster', 'Mimic',
                                    'Inept mimic', 'Ravenous Mimic', 'Ravenous mimic', 'Player ghost', 'Pandemonium lord',
                                    'Shapeshifter', 'Glowing shapeshifter', 'Serpent of Hell',
                                    'Serpent of Hell (Cocytus)', 'Serpent of Hell (Gehenna)',
                                    'Serpent of Hell (Dis)', 'Serpent of Hell (Tartarus)',
                                    'Abomination', 'Derived Undead', 'Derived undead',
                                    'Lost Soul', 'Zombie', 'Cherub', 'Chaos spawn']);
    return $r3;
}

sub filter_articles_monster_tile {
    my $articles = shift;
    my $r1 = filter_articles_string($articles, 'Category');
    my $r2 = filter_articles_string($r1, 'emonspawn');
    my $r3 = filter_articles_string($r2, 'raconian');
    my $r4 = filter_articles_obsolete_categories($r3);
    my $r5 = filter_articles_array($r4,
                                   ['Out-of-depth', 'List of monsters', 'Monster generation',
                                    'Monsters', 'User:CommanderC/Test','Monster', 'Mimic',
                                    'Inept mimic', 'Ravenous Mimic', 'Ravenous mimic', 'Player ghost', 'Pandemonium lord',
                                    'Shapeshifter', 'Glowing shapeshifter', 'Serpent of Hell',
                                    'Serpent of Hell (Cocytus)', 'Serpent of Hell (Gehenna)',
                                    'Serpent of Hell (Dis)', 'Serpent of Hell (Tartarus)',
                                    'Abomination', 'Derived Undead', 'Derived undead',
                                    'Zombie', 'Dancing weapon', 'Snaplasher vine',
                                    'Ugly thing', 'Very ugly thing',
                                    'Death Knight']);
    return $r5;
}

sub filter_articles_monster_glyph {
    my $articles = shift;
    my $r1 = filter_articles_string($articles, 'Category');
    my $r2 = filter_articles_obsolete_categories($r1);
    my $r3 = filter_articles_array($r2,
                                   ['Out-of-depth', 'List of monsters', 'Monster generation',
                                    'Monsters', 'Lightning spire', 'User:CommanderC/Test','Monster', 'Mimic',
                                    'Inept mimic', 'Ravenous Mimic', 'Ravenous mimic', 'Player ghost', 'Pandemonium lord',
                                    'Shapeshifter', 'Glowing shapeshifter', 'Serpent of Hell',
                                    'Serpent of Hell (Cocytus)', 'Serpent of Hell (Gehenna)',
                                    'Serpent of Hell (Dis)', 'Serpent of Hell (Tartarus)',
                                    'Abomination', 'Derived Undead', 'Derived undead',
                                    'Lost Soul', 'Zombie', 'Cherub', 'Chaos spawn']);
    return $r3;
}

if (0) {
sub filter_monsters {
    my $articles = shift;
    my @reject1 = ('Out-of-depth', 'List of monsters', 'Monster generation',
                   'Monsters', 'Lightning spire', 'User:CommanderC/Test','Monster', 'Mimic', 
                   'Inept mimic', 'Ravenous Mimic', 'Player ghost', 'Pandemonium lord', 
                   'Shapeshifter', 'Glowing shapeshifter', 'Serpent of Hell', 'Tengu conjurer',
                   'Zot statue', 'Abomination', 'Derived Undead', 'Derived undead',
                   'Lost Soul', 'Zombie', 'Master Blaster', 'Cherub', 'Chaos spawn');
    # Large/small abomination, hell beast

    my $article_reject2 = $mw->list ( {
        action => 'query',
        list => 'categorymembers',
        cmtitle => 'Category:Obsolete',
        cmlimit => 'max' } );
    my @reject2 = map {$_->{title}} @$article_reject2;
    my $article_reject3 = $mw->list ( {
        action => 'query',
        list => 'categorymembers',
        cmtitle => 'Category:Obsolete monsters',
        cmlimit => 'max' } );
    my @reject3 = map {$_->{title}} @$article_reject3;
    my $article_reject4 = $mw->list ( {
        action => 'query',
        list => 'categorymembers',
        cmtitle => 'Category:Obsolete articles',
        cmlimit => 'max' } );
    my @reject4 = map {$_->{title}} @$article_reject4;

    my $r = [];

    foreach (@{$articles}) {
        my $title = $_->{title};
        if (contains($title, \@reject1)) {next;}
        if (contains($title, \@reject2)) {next;}
        if (contains($title, \@reject3)) {next;}
        if (contains($title, \@reject4)) {next;}
        if (substr($title, 0, 8) eq 'Category') {next;}
        push @$r, $_;
    }
    return $r;
}
}

sub get_name {
    my $article = shift;
    my $title = $article->{title};
    $title =~ s/ \(monster\)//g;

    return $title;
} 

sub remove_flavour {
    my ($text) = @_;
    # Remove flavour template
    if ($text =~ m/\{\{flavour/ipm) {
        my $before = ${^PREMATCH};
        my $inside_and_after = ${^POSTMATCH};
        # Count number of braces. We already have two in the regexp
        my $k = 2;
        my $pos = 0;
        while ($k>0) {
          $k++ if (substr($inside_and_after, $pos, 1) eq "{");
          $k-- if (substr($inside_and_after, $pos, 1) eq "}");
          $pos++;
        }
        my $after = substr($inside_and_after, $pos);
        $text = $before . $after;
    }
    return $text;
}

sub handle_text {
    my ($text, $monster_info) = @_;
    # $text = remove_flavour($text);   
    my $insert = "<!--monster-bot-begin-->\n${monster_info}<!--monster-bot-end-->\n";
    if ($text =~ m/<!--monster-bot-begin-->(.|\n)*<!--monster-bot-end-->\n/mp) {
        $text = ${^PREMATCH} . $insert . ${^POSTMATCH};
    } else {
      #        if ($text =~ m/\{\{monster(.|\n)*\n\}\}\n/ipm) {
      if ($text =~ m/\{\{monster/ipm) {
        my $before = ${^PREMATCH};
        my $inside_and_after = ${^POSTMATCH};
        # Count number of braces. We already have two in the regexp
        my $k = 2;
        my $pos = 0;
        while ($k>0) {
          $k++ if (substr($inside_and_after, $pos, 1) eq "{");
          $k-- if (substr($inside_and_after, $pos, 1) eq "}");
          $pos++;
        }
        my $after = substr($inside_and_after, $pos);
        $text = $before . $insert . $after;
      }
    }
    return $text; 
}

sub handle_spells {
    my ($text, $spell_info) = @_;
    my $insert = "<!--spl-bot-begin-->\n==Spells==\n${spell_info}<!--spl-bot-end-->";
    if ($text =~ m/<!--spl-bot-begin-->.*<!--spl-bot-end-->/mps) {
        $text = ${^PREMATCH} . $insert . ${^POSTMATCH};
    } else {
        if ($text =~ m/== ?Tips/ipm) {
            $text = ${^PREMATCH} . $insert . "\n\n" . ${MATCH} . ${^POSTMATCH};
        } else {
            $text = $text . $insert . "\n\n";
        }
    }
    return $text; 
}


sub handle_article {
    my $article = shift;
    my $name = get_name($article);
    my $monster_info = qx{$monster_trunk $name};
    $monster_info = decode('UTF-8', $monster_info);
    return unless $monster_info;
    my $spell_info = qx{$monster_trunk -spell $name};
    $spell_info = decode('UTF-8', $spell_info);
    my $pagename = $article->{title};
    if ($debug > 1) {
      print "Downloading $pagename\n";
    }
    my $ref = $mw->get_page( { title => $pagename } );
    unless ( $ref->{missing} ) {
      if ($debug > 1) {
        print "Editing $pagename\n";
      }

      my $text = $ref->{'*'};
#        $text = decode('UTF-8', $text);
        unless($only_spells) {
          $text = handle_text($text, $monster_info);
        }
        if ($spell_info) {
          $text = handle_spells($text, $spell_info);
        }
#        $text = decode('UTF-8', $text);
        my $new_text = $text;
        if ($debug > 3) {
            print "New text:";
            print "<<$new_text>>";
        }
        if ($debug < 3) {
          my $timestamp = $ref->{timestamp};
          $mw->edit( {
              action => 'edit',
              title => $pagename,
              summary => $default_summary,
              basetimestamp => $timestamp, # to avoid edit conflicts
              text => $new_text}
                     #,{skip_encoding => 1}
          )
        }
    }
}

sub do_monster_trunk {
    my $articles = get_category_members('Category:Monsters');
    my $filtered_articles = filter_articles_monster_trunk($articles);

    for (sort @$filtered_articles) {
        print $_->{title}."\n" if $debug < 1;
        if ($debug > 3) {
            if ($_->{title} eq $debug_page) {
                handle_article($_);
                exit;
            } else {
                next;
            }
        }
        if ($skip && $_->{title} ne $skip_page) {
            next;
        } else {
            $skip = 0;
        }
        sleep $pause;
        handle_article($_);
    }
}

sub do_monster_tile {
    my $articles = get_category_members('Category:Monsters');
    my $filtered_articles = filter_articles_monster_tile($articles);
    my $outfile = '/tmp/output.png';
    #my $f = File::Spec->join($dir, $outfile);
    for (sort @$filtered_articles) {
        my $page = $_->{title};
        my $name = get_name($_);
        my @args = ($monster_tile, '-o', $outfile, $name);
        if (system(@args) == 0) {
            print "Uploading $page\n";
            if ($debug < 3) {
                my $r = $mw->edit( {
                                    'action'   => 'upload',
                                    'filename' => $page,
                                    'comment' => $default_summary,
                                    'ignorewarnings' => $ignore_warnings,
                                    'file'     => [$outfile]
                                   } );
            }
        } else {
            carp("Monster_tile failed for $name\n");
            return;
        }
    }

}

sub handle_glyph_text {
    my ($text, $monster_info) = @_;
    if ($monster_info =~ m/glyph=([^\n]*)/m) {
      my $r = $1. "<noinclude>[[Category:Monster glyph templates]]</noinclude>";
      return $r;
    }
}

sub handle_glyph_article {
    my $article = shift;
    my $name = get_name($article);
    my $monster_info = qx{$monster_trunk $name};
    return unless $monster_info;
    my $pagename = $article->{title};
    my $ref = $mw->get_page( { title => $pagename } );
    my $newpagename = "Template:Glyph/".$name;
    unless ( $ref->{missing} ) {
        my $text = $ref->{'*'};
        my $new_text = handle_glyph_text($text, $monster_info);
        return unless $new_text;
        my $timestamp = $ref->{timestamp};
        if ($debug > 3) {
            print "New text:";
            print "<<$new_text>>";
        }
        if ($debug < 3) {
            $mw->edit( {
                        action => 'edit',
                        title => $newpagename,
                        summary => $default_summary,
                        basetimestamp => $timestamp, # to avoid edit conflicts
                        text => $new_text } );
        }
    }
}

sub do_monster_glyph {
    my $articles = get_category_members('Category:Monsters');
    my $filtered_articles = filter_articles_monster_glyph($articles);

    for (sort @$filtered_articles) {
        print $_->{title}."\n" if $debug < 1;
        if ($debug > 3) {
            if ($_->{title} eq $debug_page) {
                handle_glyph_article($_);
                exit;
            } else {
                next;
            }
        }
        if ($skip && $_->{title} ne $skip_page) {
            next;
        } else {
            $skip = 0;
        }
        sleep $pause;
        handle_glyph_article($_);
    }
}

1;



