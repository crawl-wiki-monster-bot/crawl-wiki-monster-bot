#include "AppHdr.h"
#include "externs.h"
#include "directn.h"
#include "unwind.h"
#include "env.h"
#include "colour.h"
#include "dungeon.h"
#include "database.h"
#include "describe.h"
#include "options.h"
#include "initfile.h"
#include "los.h"
#include "message.h"
#include "mon-place.h"
#include "mon-util.h"
#include "version.h"
#include "view.h"
#include "los.h"
#include "maps.h"
#include "initfile.h"
#include "libutil.h"
#include "itemname.h"
#include "itemprop.h"
#include "act-iter.h"
#include "misc.h"
#include "random.h"
#include "state.h"
#include "spl-util.h"
#include "mon-cast.h"
#include "vault_monsters.h"
#include "files.h"
#include "syscalls.h"
//tiles
#include "tilemcache.h"
#include "tiletex.h"
#include "tilepick.h"
#include "rltiles/tiledef-main.h"
#include "rltiles/tiledef-player.h"
#include "rltiles/tiledef-gui.h"
#include "windowmanager.h"
#include <SDL.h>
#include <SDL_image.h>

#include "monster-tile.h"
#include "monster-tile-cb.h"

#include <sstream>
#include <set>
#include <unistd.h>



//////////////////////////////////////////////////////////////////////////
// acr.cc stuff

CLua clua(true);
CLua dlua(false);      // Lua interpreter for the dungeon builder.
crawl_environment env; // Requires dlua.
player you;
game_state crawl_state;

FILE *yyin;
int yylineno;

std::string init_file_error;    // externed in newgame.cc

char info[ INFO_SIZE ];         // messaging queue extern'd everywhere {dlb}

int stealth;                    // externed in view.cc
bool apply_berserk_penalty;     // externed in evoke.cc

void process_command(command_type) {
}

int yyparse() {
  return 0;
}

void world_reacts() {
}

//static key_recorder repeat_again_rec;

// Clockwise, around the compass from north (same order as enum RUN_DIR)
const struct coord_def Compass[9] =
{
    coord_def(0, -1), coord_def(1, -1), coord_def(1, 0), coord_def(1, 1),
    coord_def(0, 1), coord_def(-1, 1), coord_def(-1, 0), coord_def(-1, -1),
    coord_def(0, 0)
};

///////

const coord_def MONSTER_PLACE(20, 20);
const coord_def PLAYER_PLACE(21, 20);

const int PLAYER_MAXHP = 500;

static void initialize_crawl() {
  get_system_environment();
  SysEnv.crawl_dir = "crawl-ref/crawl-ref/source/";

  init_monsters();
  init_properties();
  init_item_name_cache();

  init_spell_descs();
  init_monster_symbols();
  databaseSystemInit();
  init_mon_name_cache();
  init_spell_name_cache();
  init_mons_spells();
  init_element_colours();

  dgn_reset_level();
  for (int y = 0; y < GYM; ++y)
    for (int x = 0; x < GXM; ++x)
      grd[x][y] = DNGN_FLOOR;

  los_changed();
  you.moveto(PLAYER_PLACE);
  you.hp = you.hp_max = PLAYER_MAXHP;
}

int mi_create_monster(mons_spec spec) {
  monster *monster = 
    dgn_place_monster(spec, MONSTER_PLACE, true, false, false);
  if (monster) {
    monster->behaviour = BEH_SEEK;
    monster->foe = MHITYOU;
    no_messages mx;
    monster->del_ench(ENCH_SUBMERGED);
    return monster->mindex();
  }
  return NON_MONSTER;
}

static void draco_or_demonspawn_adjust(int index)
{
    monster *mp = &menv[index];
        
    switch (mp->type)
    {
    case MONS_DRACONIAN_CALLER:
    case MONS_DRACONIAN_MONK:
    case MONS_DRACONIAN_ZEALOT:
    case MONS_DRACONIAN_SHIFTER:
    case MONS_DRACONIAN_ANNIHILATOR:
    case MONS_DRACONIAN_SCORCHER:
    case MONS_DRACONIAN_KNIGHT:
    {
        mp->base_monster = MONS_DRACONIAN;
        break;
    }
    case MONS_BLOOD_SAINT:
    case MONS_CHAOS_CHAMPION:
    case MONS_WARMONGER:
    case MONS_CORRUPTER:
    case MONS_BLACK_SUN:
    {
        mp->base_monster = MONS_DEMONSPAWN;
        break;
    }
    default:
        break;
    }
}

static bool load_texture(GenericTexture *tex, const char *filename,
                              MipMapOptions mip_opt, unsigned int &orig_width,
                              unsigned int &orig_height, tex_proc_func proc = NULL,
                         bool force_power_of_two = true);
static SDL_Surface *load_image(const char *file);

static tile_info g_tile_info(0, 0, 0, 0, 0, 0, 0, 0);

tile_size get_tile_size()
{
  tile_size t;
  t.sx = g_tile_info.sx;
  t.sy = g_tile_info.sy;
  t.ex = g_tile_info.ex;
  t.ey = g_tile_info.ey;
  t.width = g_tile_info.width;
  t.height = g_tile_info.height;
  t.offset_x = g_tile_info.offset_x;
  t.offset_y = g_tile_info.offset_y;

  return t;
}

enum target {
  TARGET_MONSTER,
  TARGET_SPELL
};

target g_target = TARGET_MONSTER;
static const char default_output_filename[] = "output.png";
const char *g_output_filename = default_output_filename;

static void usage(const char *argv0)
{
  printf("Usage: %s [-o output_file.png] [-s] [-v] <monster name>\n", argv0);
}

static int do_monster_tile(std::string const &target);
static int do_spell_tile(std::string const &target);
static int process_texture(tileidx_t const &t, tile_info_func *info_func, const char *texture_file);

int main(int argc, char *argv[])
{
  crawl_state.test = true;
  int c;
  while ((c = getopt(argc, argv, "vso:")) != -1) {
    switch (c) {
    case 'v':
      printf("Monster tiles Crawl version: %s\n", Version::Long);
      return 0;
    case 's':
      g_target = TARGET_SPELL;
      break;
    case 'o':
      g_output_filename = optarg;
      break;
    case '?':
    default:
      usage(argv[0]);
      return 0;
    }
  }
  if (!(optind < argc))
  {
    usage(argv[0]);
    return 0;
  }

  int x = optind;
  std::string target = argv[x];
  ++x;
  while (x < argc) {
      target.append(" ");
      target.append(argv[x]);
      ++x;
  }

  trim_string(target);
  std::string orig_target = std::string(target);

  initialize_crawl();

  switch (g_target) {
  case TARGET_SPELL:
    return do_spell_tile(target);
    break;
  case TARGET_MONSTER:
  default:
    return do_monster_tile(target);
  }

  return 0;
}

// See tiletex.cc and windowmanager-sdl.cc:SDLWrapper::load_texture

static int do_spell_tile(std::string const &target)
{
  spell_type spell;
  if ((spell = spell_by_name(target, false)) == SPELL_NO_SPELL) {
    printf("Unknown spell: \"%s\"\n", target.c_str());
  }
  tileidx_t t = tileidx_spell(spell) & TILE_FLAG_MASK;
  return process_texture(t, tile_gui_info, "gui.png");
}


static int do_monster_tile(std::string const &target)
{
  std::string target2 = target;
  mons_list mons;
  std::string err = mons.add_mons(target, false);
  if (!err.empty()) {
    target2 = "the " + target;
    const std::string test = mons.add_mons(target2, false);
    if (test.empty())
      err = test;
  }

  mons_spec spec = mons.get_monster(0);
  monster_type spec_type = static_cast<monster_type>(spec.type);
  //bool vault_monster = false;

  if ((spec_type < 0 || spec_type >= NUM_MONSTERS
       || spec_type == MONS_PLAYER_GHOST)
      || !err.empty())
  {
    spec = get_vault_monster(target);
    spec_type = static_cast<monster_type>(spec.type);
    if (spec_type < 0 || spec_type >= NUM_MONSTERS
        || spec_type == MONS_PLAYER_GHOST)
    {
      if (err.empty())
        printf("Unknown monster: \"%s\"\n", target.c_str());
      else
        printf("%s\n", err.c_str());
      return 1;
    }

    //vault_monster = true;
  }

  int index = mi_create_monster(spec);
  if (index < 0 || index >= MAX_MONSTERS) {
    printf("Failed to create test monster for %s\n", target.c_str());
    return 1;
  }
  draco_or_demonspawn_adjust(index);

  monster *mp = &menv[index];
  monster_info mi(mp);
  tileidx_t t   = tileidx_monster(mi) & TILE_FLAG_MASK;

  return process_texture(t, tile_player_info, "player.png");
}

static int process_texture(tileidx_t const &t, tile_info_func *info_func, const char *texture_file)
{
  // TileBuffer::add ox=0, oy=0, centre=true, ymax=-1, tile_x=TILE_X, tile_x=TILE_Y
  // TilesTexture::get_coords
  // tile_info defined in tiledef_defines.h
  // left-top (sx, sy); right-bottom (ex, ey)

  g_tile_info = info_func(t);

  // Do SDL initialization
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0) {
    printf("Failed to initialise SDL: %s\n", SDL_GetError());
    return false;
  }
  unsigned int width, height;
  load_texture(NULL, texture_file, MIPMAP_NONE, width, height, my_tex_proc, true);
  SDL_Quit();

  return 0;
}


static bool load_texture(GenericTexture *tex, const char *filename,
                              MipMapOptions mip_opt, unsigned int &orig_width,
                              unsigned int &orig_height, tex_proc_func proc,
                              bool force_power_of_two)
{
    char acBuffer[512];

    string tex_path = datafile_path(filename);

    if (tex_path.c_str()[0] == 0)
    {
        fprintf(stderr, "Couldn't find texture '%s'.\n", filename);
        return false;
    }

    SDL_Surface *img = load_image(tex_path.c_str());

    if (!img)
    {
        fprintf(stderr, "Couldn't load texture '%s'.\n", tex_path.c_str());
        return false;
    }

    unsigned int bpp = img->format->BytesPerPixel;
    //glmanager->pixelstore_unpack_alignment(bpp);

    // Determine texture format
    unsigned char *pixels = (unsigned char*)img->pixels;

    int new_width;
    int new_height;
    if (force_power_of_two)
    {
        new_width = 1;
        while (new_width < img->w)
            new_width *= 2;

        new_height = 1;
        while (new_height < img->h)
            new_height *= 2;
    }
    else
    {
        new_width = img->w;
        new_height = img->h;
    }

    // GLenum texture_format
    if (bpp == 4)
    {
        // Even if the size is the same, still go through
        // SDL_GetRGBA to put the image in the right format.
        SDL_LockSurface(img);
        pixels = new unsigned char[4 * new_width * new_height];
        memset(pixels, 0, 4 * new_width * new_height);

        int dest = 0;
        for (int y = 0; y < img->h; y++)
        {
            for (int x = 0; x < img->w; x++)
            {
                unsigned char *p = ((unsigned char*)img->pixels
                                  + y * img->pitch + x * bpp);
                unsigned int pixel = *(unsigned int*)p;
                SDL_GetRGBA(pixel, img->format, &pixels[dest],
                            &pixels[dest+1], &pixels[dest+2],
                            &pixels[dest+3]);
                dest += 4;
            }
            dest += 4 * (new_width - img->w);
        }

        SDL_UnlockSurface(img);
    }
    else if (bpp == 3)
    {
        if (new_width != img->w || new_height != img->h)
        {
            SDL_LockSurface(img);
            pixels = new unsigned char[4 * new_width * new_height];
            memset(pixels, 0, 4 * new_width * new_height);

            int dest = 0;
            for (int y = 0; y < img->h; y++)
            {
                for (int x = 0; x < img->w; x++)
                {
                    unsigned char *p = ((unsigned char*)img->pixels
                                       + y * img->pitch + x * bpp);
                    unsigned int pixel;
                    if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
                        pixel = p[0] << 16 | p[1] << 8 | p[2];
                    else
                        pixel = p[0] | p[1] << 8 | p[2] << 16;
                    SDL_GetRGBA(pixel, img->format, &pixels[dest],
                                &pixels[dest+1], &pixels[dest+2],
                                &pixels[dest+3]);
                    dest += 4;
                }
                dest += 4 * (new_width - img->w);
            }

            SDL_UnlockSurface(img);
        }
    }
    else if (bpp == 1)
    {
        // need to depalettize
        SDL_LockSurface(img);

        pixels = new unsigned char[4 * new_width * new_height];

        SDL_Palette* pal = img->format->palette;
        ASSERT(pal);
        ASSERT(pal->colors);

        int src = 0;
        int dest = 0;
        for (int y = 0; y < img->h; y++)
        {
            int x;
            for (x = 0; x < img->w; x++)
            {
                unsigned int index = ((unsigned char*)img->pixels)[src++];
                pixels[dest*4    ] = pal->colors[index].r;
                pixels[dest*4 + 1] = pal->colors[index].g;
                pixels[dest*4 + 2] = pal->colors[index].b;
                pixels[dest*4 + 3] = (index != img->format->colorkey ? 255 : 0);
                dest++;
            }
            while (x++ < new_width)
            {
                // Extend to the right with transparent pixels
                pixels[dest*4    ] = 0;
                pixels[dest*4 + 1] = 0;
                pixels[dest*4 + 2] = 0;
                pixels[dest*4 + 3] = 0;
                dest++;
            }
        }
        while (dest < new_width * new_height)
        {
            // Extend down with transparent pixels
            pixels[dest*4    ] = 0;
            pixels[dest*4 + 1] = 0;
            pixels[dest*4 + 2] = 0;
            pixels[dest*4 + 3] = 0;
            dest++;
        }

        SDL_UnlockSurface(img);
    }
    else
    {
        printf("Warning: unsupported format, bpp = %d for '%s'\n",
               bpp, acBuffer);
        return false;
    }


#if 0
    bool success = false;
    if (!proc || proc(pixels, new_width, new_height))
        success |= tex->load_texture(pixels, new_width, new_height, mip_opt);
#else
    bool success = true;
    if (proc)
      success &= proc(pixels, new_width, new_height);
#endif

    // If conversion has occurred, delete converted data.
    if (pixels != img->pixels)
        delete[] pixels;

    orig_width  = img->w;
    orig_height = img->h;

    SDL_FreeSurface(img);

    return success;
}


static SDL_Surface *load_image(const char *file)
{
    SDL_Surface *surf = NULL;
    FILE *imgfile = fopen_u(file, "rb");
    if (imgfile)
    {
        SDL_RWops *rw = SDL_RWFromFP(imgfile, 0);
        if (rw)
        {
            surf = IMG_Load_RW(rw, 0);
            SDL_RWclose(rw);
        }
        fclose(imgfile);
    }

    if (!surf)
        return NULL;

    return surf;
}
