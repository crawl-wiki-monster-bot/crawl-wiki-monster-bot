# Use 'make stable' to compile monster, 'make trunk' to compile monster-trunk.
# Use 'make install' to install monster, 'make install-trunk' to install
# Use 'make TILES=1 monster-tile' to compile monster-tile
# monster-trunk.

.PHONY: crawl

# Use master
TRUNK = master

CRAWL_PATH=crawl-ref/crawl-ref/source

CXX = g++

PYTHON = python2
ifeq (,$(wildcard /usr/bin/python2))
	PYTHON = python
endif

ifdef USE_MERGE_BASE
MERGE_BASE := $(shell cd $(CRAWL_PATH) ; git merge-base HEAD $(USE_MERGE_BASE))
endif

VERSION = $(shell cd $(CRAWL_PATH) ; git describe)

DEBUG=1

CFLAGS = -std=c++11 -Wall -Wno-parentheses -DNDEBUG -DUNIX -I$(CRAWL_PATH) \
	-I$(CRAWL_PATH)/rltiles -I/usr/include/ncursesw -g


# Fonts.
ifdef TILES

OUR_PROPORTIONAL_FONT=DejaVuSans.ttf
OUR_MONOSPACED_FONT=DejaVuSansMono.ttf

ifdef PROPORTIONAL_FONT
  DEFINES += -DPROPORTIONAL_FONT=\"$(PROPORTIONAL_FONT)\"
  ifneq (,$(COPY_FONTS))
    INSTALL_FONTS += $(PROPORTIONAL_FONT)
  endif
else
  SYS_PROPORTIONAL_FONT = $(shell dir=/usr/share/fonts; [ -d $$dir ] && find $$dir -iname $(OUR_PROPORTIONAL_FONT)|head -n 1)
  ifeq (,$(SYS_PROPORTIONAL_FONT))
    SYS_PROPORTIONAL_FONT = $(shell dir=/usr/local/share/fonts ; [ -d $$dir ] && find $$dir -iname $(OUR_PROPORTIONAL_FONT)|head -n 1)
  endif
  ifneq (,$(SYS_PROPORTIONAL_FONT))
    ifeq (,$(COPY_FONTS))
      DEFINES += -DPROPORTIONAL_FONT=\"$(SYS_PROPORTIONAL_FONT)\"
    else
      DEFINES += -DPROPORTIONAL_FONT=\"$(FONTDIR)$(OUR_PROPORTIONAL_FONT)\"
      INSTALL_FONTS += $(SYS_PROPORTIONAL_FONT)
    endif
  else
    DEFINES += -DPROPORTIONAL_FONT=\"$(FONTDIR)$(OUR_PROPORTIONAL_FONT)\"
    INSTALL_FONTS += contrib/fonts/$(OUR_PROPORTIONAL_FONT)
  endif
endif

ifdef MONOSPACED_FONT
  DEFINES += -DMONOSPACED_FONT=\"$(MONOSPACED_FONT)\"
  ifneq (,$(COPY_FONTS))
    INSTALL_FONTS += $(MONOSPACED_FONT)
  endif
else
  SYS_MONOSPACED_FONT = $(shell dir=/usr/share/fonts; [ -d $$dir ] && find $$dir -iname $(OUR_MONOSPACED_FONT)|head -n 1)
  ifeq (,$(SYS_MONOSPACED_FONT))
    SYS_MONOSPACED_FONT = $(shell dir=/usr/local/share/fonts; [ -d $$dir ] && find $$dir -iname $(OUR_MONOSPACED_FONT)|head -n 1)
  endif
  ifneq (,$(SYS_MONOSPACED_FONT))
    ifeq (,$(COPY_FONTS))
      DEFINES += -DMONOSPACED_FONT=\"$(SYS_MONOSPACED_FONT)\"
    else
      DEFINES += -DMONOSPACED_FONT=\"$(FONTDIR)$(OUR_MONOSPACED_FONT)\"
      INSTALL_FONTS += $(SYS_MONOSPACED_FONT)
    endif
  else
    DEFINES += -DMONOSPACED_FONT=\"$(FONTDIR)$(OUR_MONOSPACED_FONT)\"
    INSTALL_FONTS += contrib/fonts/$(OUR_MONOSPACED_FONT)
  endif
endif

endif


ifdef TILES
        CFLAGS += -DUSE_TILE -DUSE_TILE_LOCAL $(DEFINES) -I/usr/include/SDL
        LFLAGS = -lGL -lGLU -lz -pthread -g -lpng16
else
        LFLAGS = -lncursesw -lz -lpthread -g
endif

LUA_INCLUDE_DIR = /usr/include/lua5.1

ifeq (,$(wildcard $(LUA_INCLUDE_DIR)/lua.h))
	BUILD_LUA = y
endif

ifeq (,$(BUILD_LUA))
	CFLAGS += -I$(LUA_INCLUDE_DIR)
	LFLAGS += -llua5.1
else
	LUASRC := $(CRAWL_PATH)/contrib/lua/src
	LUALIB = lua
	LUALIBA = lib$(LUALIB).a
	CFLAGS += -I$(LUASRC)
	LFLAGS += $(LUASRC)/$(LUALIBA)
	CONTRIB_OBJECTS += $(LUASRC)/$(LUALIBA)
endif

SQLITE_INCLUDE_DIR = /usr/include

ifeq (,$(wildcard $(SQLITE_INCLUDE_DIR)/sqlite3.h))
	BUILD_SQLITE = y
endif

ifeq (,$(BUILD_SQLITE))
	LFLAGS += -lsqlite3
else
	SQLSRC := $(CRAWL_PATH)/contrib/sqlite
	SQLLIB   := sqlite3
	SQLLIBA  := lib$(SQLLIB).a
	FSQLLIBA := $(SQLSRC)/$(SQLLIBA)
	CFLAGS += -I$(SQLSRC)
	LFLAGS += $(FSQLLIBA)
	CONTRIB_OBJECTS += $(FSQLLIBA)
endif


ifdef TILES
        FREETYPE_LDFLAGS := $(shell $(PKGCONFIG) freetype2 --libs-only-L) $(shell $(PKGCONFIG) freetype2 --libs-only-l)
        SDL_LDFLAGS := $(shell $(PKGCONFIG) sdl --libs-only-L) $(shell $(PKGCONFIG) sdl --libs-only-l)
        LFLAGS += -lSDL_image -lSDL -lfreetype $(SDL_LDFLAGS) $(FREETYPE_LDFLAGS)
endif


include $(CRAWL_PATH)/Makefile.obj

CRAWL_OBJECTS := $(OBJECTS:main.o=)
ifdef TILES
        CRAWL_OBJECTS += $(TILES_OBJECTS) $(GLTILES_OBJECTS)
else 
        CRAWL_OBJECTS += libunix.o
endif
CRAWL_OBJECTS += version.o
TILEDEFS := floor wall feat main player gui icons dngn unrand
CRAWL_OBJECTS += $(TILEDEFS:%=rltiles/tiledef-%.o)


MONSTER_OBJECTS = monster-main.o vault_monster_data.o vault_monsters.o
MONSTER_TILE_OBJECTS = monster-tile.o monster-tile-cb.o vault_monster_data.o vault_monsters.o 
MONSTER_TILE_OBJECTS += $(CRAWL_PATH)/rltiles/tool/tile_colour.o
ALL_OBJECTS = $(MONSTER_OBJECTS) $(CRAWL_OBJECTS:%=$(CRAWL_PATH)/%)
ALL_TILE_OBJECTS = $(MONSTER_TILE_OBJECTS) $(CRAWL_OBJECTS:%=$(CRAWL_PATH)/%)

all: vaults trunk

crawl:
	+${MAKE} -C $(CRAWL_PATH) DEBUG=$(DEBUG) TILES=$(TILES) NO_LUA_BINDINGS=y

.cc.o:
	${CXX} ${CFLAGS} -o $@ -c $<

vault_monster_data.o: vaults

trunk: monster-trunk

vault_monster_data.o:
	${CXX} ${CFLAGS} -o vault_monster_data.o -c vault_monster_data.cc

vaults: | update-cdo-git
	rm -f vault_monster_data.cc vault_monster_data.o
	${PYTHON} parse_des.py --verbose

update-cdo-git:
	[ "`hostname`" != "ipx14623" ] || sudo -H -u git /var/cache/git/crawl-ref.git/update.sh

monster-trunk: vaults update-cdo-git crawl $(MONSTER_OBJECTS) $(CONTRIB_OBJECTS)
	g++ $(CFLAGS) -o $@ $(ALL_OBJECTS) $(LFLAGS)

monster-tile: vaults update-cdo-git crawl $(MONSTER_TILE_OBJECTS) $(CONTRIB_OBJECTS)
	g++ $(CFLAGS) -o $@ $(ALL_TILE_OBJECTS) $(LFLAGS)

$(LUASRC)/$(LUALIBA):
	echo Building Lua...
	cd $(LUASRC) && $(MAKE) all

$(FSQLLIBA):
	echo Building SQLite
	cd $(SQLSRC) && $(MAKE)

test: monster
	./monster-trunk quasit

install-trunk: monster-trunk tile_info.txt
	strip -s monster-trunk
	cp monster-trunk $(HOME)/bin/
	if [ -f ~/source/announcements.log ]; then \
	  echo 'Monster database of master branch on crawl.develz.org updated to: $(VERSION)' >>~/source/announcements.log;\
	fi

tile_info.txt:
	${PYTHON} parse_tiles.py --verbose

clean:
	rm -f *.o
	rm -f monster monster-trunk
	rm -f *.pyc vault_monster_data.cc
	cd $(CRAWL_PATH) && git clean -f -d -x && git pull
