#!/usr/bin/perl

# The location of the monster-trunk executable
my $monster_trunk = "wikibot013";

# Set the pause in seconds after each upload
#my $pause = 60;
my $pause = 10;

# List the wiki PHP scripts where you have the username/password pair
my $api_url = 'http://crawl.chaosforge.org/api.php';
my $upload_url = 'http://crawl.chaosforge.org/Special:Upload';

#Then run the script on the command line using
#
# $ perl image.pl dirname
#
# where dirname/ is the name of a directory containing the files to
# be uploaded

use strict;
use warnings;
use English;
use MediaWiki::API;
use File::Spec;
use IO::Dir;
use IO::Prompt;

my $username = prompt('Username:');
my $password = prompt('Password:', -e => '*');

my $mw = MediaWiki::API->new( {
    api_url => $api_url,
    upload_url => $upload_url,
    on_error => \&on_error
} );  
my $r = $mw->login( { lgname => $username, lgpassword => $password } );
die unless defined($r);

sub on_error {
  print "Error code: " . $mw->{error}->{code} . "\n";
  print $mw->{error}->{stacktrace}."\n";
  die;
}

sub print_articles {
    my $articles = shift;
    # and print the article titles
    foreach (@{$articles}) {
        print "$_->{title}\n";
    }
}

sub contains {
    my ($el, $arr) = @_;
    for (@$arr) {
        if ($el eq $_) {return 1;}
    }
    return 0;
}

sub filter_monsters {
    my $articles = shift;
    my @reject1 = ('Out-of-depth', 'List of monsters', 'Monster generation',
'Monsters', 'Lightning spire', 'User:CommanderC/Test','Monster', 'Mimic', 'Player ghost', 'Pandemonium lord', 'Shapeshifter', 'Glowing shapeshifter', 'Serpent of Hell', 'Tengu Conjurer', 'Zot statue');
    # Large/small abomination, hell beast

    my $article_reject2 = $mw->list ( {
        action => 'query',
        list => 'categorymembers',
        cmtitle => 'Category:Obsolete',
        cmlimit => 'max' } );
    my @reject2 = map {$_->{title}} @$article_reject2;
    my $article_reject3 = $mw->list ( {
        action => 'query',
        list => 'categorymembers',
        cmtitle => 'Category:Obsolete monsters',
        cmlimit => 'max' } );
    my @reject3 = map {$_->{title}} @$article_reject3;
    my $r = [];
    my $debug = 0;

    foreach (@{$articles}) {
        my $title = $_->{title};
        if ($debug) {
            if ($title ne 'Aizul') {next;}
        }
        if (contains($title, \@reject1)) {next;}
        if (contains($title, \@reject2)) {next;}
        if (contains($title, \@reject3)) {next;}
        if (substr($title, 0, 8) eq 'Category') {next;}
        push @$r, $_;
    }
    return $r;
}

sub get_name {
    my $article = shift;
    my $title = $article->{title};
    $title =~ s/ \(monster\)//g;

    return $title;
} 

sub handle_text {
    my ($text, $monster_info) = @_;
    if ($monster_info =~ m/glyph=([^\n]*)/m) {
      my $r = $1. "<noinclude>[[Category:Monster glyph templates]]</noinclude>";
      return $r;
    }
}

sub handle_article {
    my $article = shift;
    my $name = get_name($article);
    my $monster_info = qx{$monster_trunk $name};
    if (0) {
        print ">>" . $name . "<<\n";
        print "--" . $monster_info . "--\n";
    }
    return unless $monster_info;
    my $pagename = $article->{title};
    my $ref = $mw->get_page( { title => $pagename } );
    my $newpagename = "Template:Glyph/".$name;
    unless ( $ref->{missing} ) {
        my $text = $ref->{'*'};
        my $new_text = handle_text($text, $monster_info);
        return unless $new_text;
        my $timestamp = $ref->{timestamp};
        if (0) {
          print $new_text;
        } else {
          $mw->edit( {
                      action => 'edit',
                      title => $newpagename,
                      basetimestamp => $timestamp, # to avoid edit conflicts
                      text => $new_text } );
        }
    }
}


my $articles = $mw->list ( {
    action => 'query',
    list => 'categorymembers',
    cmtitle => 'Category:Monsters',
    cmlimit => 'max' } );

my $filtered_articles = filter_monsters($articles);
#print_articles($articles);
#print_articles($filtered_articles);
my $skip = 1;
for (sort @$filtered_articles) {
  print $_->{title}."\n";
  if ($skip && $_->{title} ne 'Yak') {
    next;
  } else {
    $skip = 0;
  }
  #sleep $pause;
  next if ($_->{title} eq 'Yak');
  handle_article($_);
}

$mw->logout();

