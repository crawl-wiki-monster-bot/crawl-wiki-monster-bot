#!/bin/sh

test () {
    ../../parse-spl-data "${2}" ./ > tmp.txt
    diff -q "${1}.txt" tmp.txt
    if [ "$?" = 1 ]; then
        echo "Test failed: ${1}"
        exit
    fi
    rm tmp.txt
}

test by_letter -a
test by_book -b
test by_flag -f
test by_level -l
test by_school -s
test table_of_spellbooks -m
test table_of_spells -k
echo "All tests passed"

