#include <stdio.h>
#include <string>
#include <cassert>
#include <cstring>
#include <stdint.h>
#if 0
#include <cstdlib>
#include <png.h>
#endif
#include "rltiles/tool/tile_colour.h"
#include "monster-tile.h"


bool my_tex_proc(unsigned char *pixels, unsigned int w, unsigned int h)
{
  // g_tile_info has the coords
  // pixels has w*h pixels, 4bpp, by rows
  //see tile_page::write_image in rltiles/tool/tile_page.cc

  const unsigned bpp = 4;
  assert(bpp == sizeof(tile_colour));

  if (w * h <= 0) {
    fprintf(stderr, "Error: failed to write image.\n");
    return false;
  }

  tile_size ts = get_tile_size();
  const size_t dest_size = ts.width * ts.height;
  tile_colour *dest_pixels = new tile_colour[dest_size];
  memset(dest_pixels, 0, dest_size * sizeof(tile_colour));
  unsigned pad_x = ts.width - (ts.ex - ts.sx);
  unsigned extra1_x = ts.offset_x;
  unsigned extra2_x = pad_x - extra1_x;
  //unsigned pad_y = ts.height - (ts.ey - ts.sy);
  unsigned extra1_y = ts.offset_y;
  //unsigned extra2_y = pad_y - extra1_y;

  unsigned out = 0 + ts.width*extra1_y + extra1_x;
  for (unsigned j = ts.sy; j < ts.ey; ++j) {
    for (unsigned i = ts.sx; i < ts.ex; ++i) {
      unsigned char *pix = pixels+j*w*bpp+i*bpp;
      if (out < 0 || out >= dest_size) {
        fprintf(stderr, "Index out of bounds in my_tex_proc\n");
        return 0;
      }
      dest_pixels[out].a = pix[3];
      dest_pixels[out].b = pix[2];
      dest_pixels[out].g = pix[1];
      dest_pixels[out].r = pix[0];
      ++out;
    }
    out += extra2_x + extra1_x;
  }
  bool success = write_png(g_output_filename, dest_pixels, ts.width, ts.height);
  delete[] dest_pixels;
  
  return success;
}
